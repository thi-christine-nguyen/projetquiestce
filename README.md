# Lancement du jeu

            Pour un lancement plus facile de notre jeu nous avons decidé de faire un MakeFile.
            Il vous suffira de lancer le MakeFile depuis votre terminal. 
            Si vous n'arrivez pas à lancer le MakeFile, vous pouvez lancer le jeu avec l'extension(.py). 

            Pour cela, vous devez télécharger la bibliothèque pygame : 
                Pour installer pygame:

                Sur windows:
                -  python -m pip install pygame

                Sur linux:
                -sudo apt-get install python3-pip (passer directement à la dernière étape si pip3 est déjà installer)
                -sudo pip3 install -update pip
                -sudo pip3 install pygame


                Il est préférable d'avoir la version 3.10.2 de python  "https://www.python.org/downloads/release/python-3102/".

            Après téléchargement de pygame, veuillez executer le fichier "main.py"



# Format Json

Parlant du format Json notre projet, nous avons décidé de nous inspirer du fichier JSON fourni en exemple lors de la présentation du projet.
Nous y avons toutefois ajouté ou modifié quelques attributs afin de facilités la gestion des fichiers par notre programme:

- type: cet attribut est également le nom du fichier, il nous permet de savoir de quel type sont les objets que l'on cherche à découvrir (ex: personnages, animaux, drapeaux...)

- images: il s'agit du dossier qui contient les images apparaissant dans la grille

- categories: cet attribut est une liste de toutes les catégories qui sont utilisées pour décrire les objets à trouver. Par exemple, pour un personnage, on peut s'interroger sur la couleur de ses cheveux ou s'il a des lunettes, mais pas s'il a des plumes.

- pour chaque catégorie, il existe un attribut qui est une liste contenant les valeurs possibles pour cette catégorie. Ainsi, lors de la création de la grille, l'utilisateur peut indiquer que les personnages ont soin des lunettes, soit n'en ont pas. Alors l'attribut "lunettes" pourra prendre les valeur "oui" ou "non", et seulement ces valeurs.

- noms: cet attribut est une liste contenant les noms de tous les objets à trouver dans la grille. Il doit donc contenir autant d'éléments qu'il y a de cases dans la grille.

- possibilites: cet objet contient une liste d'objets décrivant chaque case de la grille. Chaque case est identifiée par un nombre. Chaque objet indique le nom associé à la case, le nom du fichier correspondant à l'image associée à la case, ainsi que la valeur de chaque catégorie pour cette case.

# Le générateur

Installation:

Notre générateur dispose d'un Makefile. Il suffit donc d'ouvrir un terminal dans le dossier dans lequel sont enregistrés les fichiers, et de taper la commande make. Une fenêtre s'ouvre alors, qui permet de créer un nouveau fichier Json compatible avec notre jeu.

Mode d'emploi du générateur:
 
Les images que vous souhaitez utiliser pour votre grille doivent se trouver dans le dossier QuiEstCe/images, dans un dossier portant le nom de votre grille. 
Lorsque le générateur s'ouvre, il vous demande de nommer votre grille. Nous vous conseillons de choisir un nom au singulier.
Puis, en allant sur l'onglet catégorie, vous allez pouvoir ajouter les différentes catégories décrivant chaque case de votre grille. Deux zones de saisie apparaissent alors sur l'écran: la zone supérieure vous permettra de saisir le nom de la catégorie; la zone inférieure de saisir les valeurs possibles pour cette catégorie. Vous devrez valider chaque possibilité. Lorsque que vous avez saisi toutes les valeurs possibles pour une catégorie, vous pouvez créer une nouvelle catégorie en cliquant sur le bouton "+" situé à côté du nom de vos catégories déjà créées. Un bouton "Nouvelle categorie" apparaît alors. Il faut cliquer sur ce bouton, et vous pourrez répéter l'opération et créer une nouvelle catégorie. 
Une fois toutes vos catégories créées, il est temps de passer à la création de chaque case. Pour cela, il faut cliquer sur le bouton "Case" situé en haut de la fenêtre, puis sur le bouton "Nouvelle case". Cela vous emmène sur un nouvel onglet. Cet onglet dispose d'une zone de saisie vous permettant d'entrer le nom de votre case. Le bouton "Parcourir les fichiers" vous permet de selectionner l'image que vous souhaitez associer à votre case, à partir de votre explorateur de fichiers. Si vous déplacez par la suite cette image, votre grille ne fonctionnera pas. Nous vous conseillons donc de bien choisir où vous souhaitez placer vos images, avant de créer votre grille. Sous ce bouton, il y a autant de listes déroulantes que de catégories. Ces listes déroulantes vous permettent de choisir la valeur voulue pour chaque catégorie. Vous pouvez saisir plusieurs valeurs par catégorie, si besoin. Lorsque que vous avez saisi au moins une valeur pour chaque catégorie, vous pouvez cliquer sur le bouton "Valider", ce qui crée votre case. Puis, vous pourrez créer une nouvelle case en appuyant sur le bouton "+" en haut de la fenêtre, puis sur le bouton "Nouvelle Case" qui apparaît. Attention! Si vous avez saisi pour chaque catégorie, des valeurs identiques à celles décrivant une case déjà existante, vous recevrez un message d'erreur, et il vous sera demandé de relancer le générateur, et de recommencer le processus. Nous vous conseillons donc de vérifier à l'avance si les catégories que vous avez choisies vous permettent de décrire chaque personnage de manière unique.
Enfin, lorsque vous avez créé toutes les cases que vous souhaitiez, vous pouvez cliquer sur le bouton "Validation". Cela crée le fichier Json décrivant la grille que vous venez de créer et l'enregistre dans le dossier FichiersJson du jeu.

Amusez-vous bien!
