from Jeu import Jeu
from fpg_utils.utils import *
import pygame
from pygame.locals import *
import random
import platform

from grille import * 
from function import *


slash=""
if platform.system()=="Windows":
    slash="\\"
else:
    slash="/"

class card_class(button_class):
    def __init__(self, x_pos, y_pos,
                 surfaces: button_surfaces,
                 text=None, text_color=(0,0,0), text_font=button_font,
                 action=lambda s: print("Button was clicked."),
                 hover_cursor = pygame.cursors.Cursor(pygame.SYSTEM_CURSOR_HAND),
                 action_on_pressed=False,
                 name = "",
                checked = False,
                cache = None):
        button_class.__init__(self, x_pos, y_pos,
                 surfaces,
                 text, text_color, text_font,
                 action,
                 hover_cursor,
                 action_on_pressed)
        self.checked = checked
        self.name = name
        self.cache = cache

class named_button(button_class):
    def __init__(self, x_pos, y_pos,
                 surfaces: button_surfaces,
                 text=None, text_color=(255, 255, 255), text_font=button_font,
                 action=lambda s: print("Button was clicked."),
                 hover_cursor = pygame.cursors.Cursor(pygame.SYSTEM_CURSOR_HAND),
                 action_on_pressed=False,
                 name = ""):
        button_class.__init__(self, x_pos, y_pos,
                 surfaces,
                 text, text_color, text_font,
                 action,
                 hover_cursor,
                 action_on_pressed)
        self.name = name

def mask(s) :
    s.surfaces.surface.set_alpha(100)

def change_opacity(s):
    if s.transparent == False :
        s.surfaces.surface.set_alpha(100)
        s.transparent = True
    else :
        s.surfaces.surface.set_alpha(0)
        s.transparent = False

class cache_class(button_class):
    def __init__(self, x_pos, y_pos,
                 surfaces: button_surfaces,
                 text=None, text_color=(255, 255, 255), text_font=button_font,
                 action=change_opacity,
                 hover_cursor = pygame.cursors.Cursor(pygame.SYSTEM_CURSOR_HAND),
                 action_on_pressed=False,
                 transparent = False,
                 name = ""):
        button_class.__init__(self, x_pos, y_pos,
                 surfaces,
                 text, text_color, text_font,
                 action,
                 hover_cursor,
                 action_on_pressed)
        self.surfaces.surface.set_alpha(0)
        self.surfaces.hover_surface.set_alpha(150)
        self.transparent = transparent

        


class change_part_class():
    def __init__(self, part_list, button):
        self.part_list = part_list
        self.button = button
    

    def __call__(self, widget):
        index = self.part_list.index(widget.text)
        next_index = index + 1
        if next_index >= len(self.part_list): next_index = 0
        widget.text = self.part_list[next_index]
        self.button.part_selection_dict[self.button.text] = widget.text
        


class graphic_motor :
    def __init__(self) :

        #chargement de la musique dans le mixer et lecture de la playlist en boucle
        pygame.mixer.music.set_volume(0.1)
        pygame.mixer.music.load(resource_path('Ressources'+ slash +'backgroundMusic.mp3'))
        pygame.mixer.music.play(loops = -1)

        self.playing_music = True
        self.generated = False
        self.jeu = ""
        self.grilleActuelle = ""
        self.allCategories = ""
        self.liste_answers = []
        self.attByCat=[]
        self.attByCat2=[]
        self.saves = ""

        self.cards = []
        
        
        self.used_grille = 0
        self.used_mode = 0

        self.connector = 0
        self.double = 0
        self.selected_answer = 0
        self.selected_answer2 = 0 
        self.selected_category=0
        self.attribut=0
        self.selected_category2=0
        self.attribut2=0


        self.party_loaded = ""
        


        #Fonction de remise à zero de tous les attributs pour nouvelle partie
        def remiseAZero() : 
            #remise à zero de tous les attributs
            self.jeu = ""
            self.grilleActuelle = ""
            self.allCategories = ""
            self.liste_answers = []
            self.attByCat=[]
            self.attByCat2=[]
            self.saves = ""
            self.used_grille = 0
            self.used_mode = 0
            self.connector = 0
            self.double = 0
            self.selected_answer = 0
            self.selected_answer2 = 0 
            self.selected_category=0
            self.attribut=0
            self.selected_category2=0
            self.attribut2=0
            self.party_loaded = ""
            self.cards=[]
        
            
            
            #remise à zero des boutons et textes
            load_button_1.text='Vide'
            load_button_2.text='Vide'
            load_button_3.text='Vide'
            load_button_4.text='Vide'
            load_button_5.text='Vide'
            nomSauvegarde.text = ""
            annonce_text_element.text =""
            response_text_element.text = ""
            connector_button.text = ""
            answer_text_element2.text = ""
            answer_text_element.text = ""
            select_category_button_1.text=""
            select_attribut_button_1.text=""
            select_category_button_2.text=""
            select_attribut_button_2.text=""
            if select_category_button_2 in scenes[1].widgets :
                    scenes[1].widgets.remove(select_category_button_2)
            if select_attribut_button_2 in scenes[1].widgets :
                    scenes[1].widgets.remove(select_attribut_button_2)
        
            scenes[1].widgets = [select_category_button_1, select_attribut_button_1,connector_button, validate_question_button, select_answer_button, send_answer_button, giveup_button, sound_button, save_party_button]
            scenes[2].widgets = [select_category_button_1, select_attribut_button_1,connector_button, validate_question_button, select_answer_button, send_answer_button, giveup_button, sound_button, save_party_button,estimate_button]
            scenes[3].widgets=[select_category_button_1, select_attribut_button_1,connector_button, validate_question_button, select_answer_button,select_answer_button2, send_answer_button, giveup_button, sound_button, save_party_button, double_button]
            scenes[4].widgets = [select_category_button_1, select_attribut_button_1,connector_button, validate_question_button, select_answer_button, send_answer_button, giveup_button, sound_button, save_party_button]
            
        def card_surface(image_path, l=128, h=64 ):
            

            return pygame.transform.smoothscale(pygame.image.load(image_path).convert_alpha(), (l, h))

        def press_card(s) :
            if (s.checked.croix == True) :
                s.checked.croix = False

            else : 
                s.checked.croix = True
                
        
        def generate_grid(grille) :
            
            indice = 0


            if not self.generated :
                self.generated = True
                for row in range(int(grille.nombreLignes)) :
                    for col in range(int(grille.nombreColonnes)):
                        posx = 0.15+(col*(0.6/(int(grille.nombreColonnes)+1)))
                        posy = 0.2+(row*(0.8/(int(grille.nombreLignes)+1)))
                        #path = images/" + grille.type + "/" + grille.grille[indice].cheminImage
                        #path = resource_path(grille.grille[indice].cheminImage)
                        path = resource_path("images" + slash + grille.type + slash + grille.grille[indice].cheminImage)
                        #print("chemin",resource_path(grille.grille[indice]))
                        cache = cache_class(posx, posy,
                                        button_surfaces(button_surface(l=120,h=140),
                                                        button_surface(l=120,h=140, outline_color=GREEN)
                                                        )
                                        )
                        if grille.grille[indice].croix == True :  
                            change_opacity (cache)
                        
                        card = card_class(posx, posy,
                                    button_surfaces(card_surface(path, l=120, h=140),
                                                    card_surface(path, l=120, h=140)
                                                    ),
                                    text =  grille.grille[indice].nom,
                                    action = press_card,
                                    checked = grille.grille[indice],
                                    cache = cache,
                                    name = grille.grille[indice].nom
                                    )
                        
                        
                        #label = text_label_class(posx, posy-1, text=card.name, text_color=ALMOST_BLACK)
                        scenes[1].widgets.append(card)
                        scenes[1].widgets.append(cache)
                        self.cards.append(card)

                        scenes[2].widgets.append(card)
                        scenes[2].widgets.append(cache)

                        scenes[3].widgets.append(card)
                        scenes[3].widgets.append(cache)

                        scenes[4].widgets.append(card)
                        scenes[4].widgets.append(cache)
                        indice += 1

        #Gestion musique 
        def music_gestion() :
            if self.playing_music == True :
                pygame.mixer.music.pause()
                self.playing_music = False
            else :
                pygame.mixer.music.unpause()
                self.playing_music = True



        sound_button = button_class(0.05, 0.1,
                                button_surfaces(button_surface(l=80,h=80),
                                                button_surface(l=80,h=80, outline_color=GREEN)
                                                ),
                                text = 'Mute',
                                action = lambda s: music_gestion()
                                )
        
        

       #Page du menu 

        title_element = text_label_class(0.5, 0.1, text='Qui est-ce ?', text_color=ALMOST_BLACK)
        label_grille_element = text_label_class(0.2, 0.4, text='Choisissez votre grille', text_color=ALMOST_BLACK)
        label_mode_element = text_label_class(0.8, 0.4, text='Choisissez votre mode de jeu', text_color=ALMOST_BLACK)

        liste_grilles = getPath(resource_path("FichiersJson")) #Récupère tous les fichiers json (pour la formation de la grille)
        liste_modes = getNomMode() #Récupère tous les noms des modes
        
        


        def change_grille(s):
            
            if self.used_grille >= len(liste_grilles)-1 :
                self.used_grille = 0
            else : self.used_grille +=1

            
            s.name = liste_grilles[self.used_grille]
            s.text = liste_grilles[self.used_grille].capitalize()

           
            

        grille_button = named_button(0.2, 0.5,
                                    button_surfaces(button_surface(l=230)),
                                    text = liste_grilles[self.used_grille].capitalize(),
                                    action = change_grille,
                                    name = liste_grilles[self.used_grille]
                                    )
        
        def change_mode(s):
            if self.used_mode >= len(liste_modes)-1 :
                self.used_mode = 0
            else : self.used_mode +=1
            
            s.name = liste_modes[self.used_mode]
            s.text = liste_modes[self.used_mode].capitalize()


        mode_button = named_button(0.8, 0.5,
                                    button_surfaces(button_surface()),
                                    text = liste_modes[self.used_mode].capitalize(),
                                    action = change_mode, 
                                    name = liste_modes[self.used_mode]
                                    )


        #Mode de selection de la grille et du mode au hasard
        def random_change(s) :
            rand_grille = random.randint(0,len(liste_grilles)-1)
            rand_mod = random.randint(0,len(liste_modes)-1)
            grille_button.text = liste_grilles[rand_grille].capitalize()
            grille_button.name =  liste_grilles[rand_grille]
            self.used_grille = rand_grille
            mode_button.text = liste_modes[rand_mod].capitalize()
            mode_button.name = liste_modes[rand_mod]

            self.used_mode = rand_mod


        random_button = button_class(0.5, 0.5,
                                    button_surfaces(button_surface(l=170)),
                                    text = 'Aléatoire',
                                    action = random_change
                                    )
            
        def initializeGame() : 
            
            used_mode = self.used_mode
            self.generated = False

            self.grilleActuelle = self.jeu.objetGrille
            self.allCategories = self.grilleActuelle.getAllCategories()
            
            self.attByCat=self.grilleActuelle.getAllAttributsByCategorie(self.allCategories[self.selected_category])
            self.attByCat2=self.grilleActuelle.getAllAttributsByCategorie(self.allCategories[self.selected_category2])
            self.liste_answers = self.grilleActuelle.getAllNoms()
            select_answer_button.text = self.liste_answers[self.selected_answer]
            answer_text_element.text = self.grilleActuelle.caseChoisie[0].getNom() 
            print(answer_text_element.text)
            
            select_category_button_1.text=self.allCategories[self.selected_category]
            select_category_button_2.text=self.allCategories[self.selected_category2]
            select_attribut_button_1.text=self.attByCat[self.attribut]
            select_attribut_button_2.text=self.attByCat2[self.attribut2]

            generate_grid(self.grilleActuelle)
            grille_text_element.text = grille_button.name
            

            if (mode_button.name == 'triche') :
               
                
                scene_manager.goto_scene('game_triche')

            elif mode_button.name == 'double' : 
                
               
                answer_text_element2.text = self.grilleActuelle.caseChoisie[1].getNom()
                select_answer_button2.text = self.liste_answers[self.selected_answer2]
                
                print(answer_text_element2.text)

                scene_manager.goto_scene('double')

            elif mode_button == "facile" : 
                scene_manager.goto_scene('facile')

            else :
                
                scene_manager.goto_scene('game')
            
            self.used_mode = used_mode
            
        
           

        #Fonction d'initialisation des attribut selon la partie
        def start_game(s) :
            remiseAZero()

            #Nouvelle partie    
            if s.text == "START" : 
                self.jeu = Jeu(resource_path("FichiersJson/" + grille_button.name), mode_button.name)
                
                initializeGame()
            #Chargement partie sauvegardée
            else : 
                self.saves = getPath(resource_path("Saves"))
                load_save()
                self.party_loaded = s.text 
                try : 
                    rep = reprendre(self.party_loaded)
                    self.jeu = Jeu(rep[0], rep[1])
                    initializeGame()
                except TypeError :  
                    print("Pas de sauvegarde")
                    scene_manager.goto_scene("load")
                    
                
        

        start_button = button_class(0.5, 0.8,
                                button_surfaces(button_surface(l=160),
                                                button_surface(l=160,outline_color=GREEN)
                                                ),
                                text = 'START',
                                action = start_game
                                )


        
       
        

        #Page menu sauvegarde 
        self.saves = getPath(resource_path("Saves"))


        load_button_1 = button_class(0.5, 0.3,
                                button_surfaces(button_surface(l=200),
                                                button_surface(l=200,outline_color=GREEN)
                                                ),
                                text = 'Vide',
                                action = start_game
                                )
        load_button_2 = button_class(0.5, 0.4,
                                button_surfaces(button_surface(l=200),
                                                button_surface(l=200,outline_color=GREEN)
                                                ),
                                text = 'Vide',
                                action = start_game
                                )
        load_button_3 = button_class(0.5, 0.5,
                                button_surfaces(button_surface(l=200),
                                                button_surface(l=200,outline_color=GREEN)
                                                ),
                                text = 'Vide',
                                action = start_game
                                )
        load_button_4 = button_class(0.5, 0.6,
                                button_surfaces(button_surface(l=200),
                                                button_surface(l=200,outline_color=GREEN)
                                                ),
                                text = 'Vide',
                                action = start_game
                                )
        load_button_5 = button_class(0.5, 0.7,
                                button_surfaces(button_surface(l=200),
                                                button_surface(l=200,outline_color=GREEN)
                                                ),
                                text = 'Vide',
                                action = start_game
                                )
        
        buttons = [load_button_1, load_button_2, load_button_3, load_button_4, load_button_5]

        def load_save() :
            self.saves = getPath(resource_path("Saves"))
            if len(self.saves)<5 :
                nb_saves = len(self.saves)
            else :
                nb_saves = 5
                
            for i in range(nb_saves) :
                if buttons[i].text != self.saves[i] :
                    buttons[i].text = self.saves[i]

            
        
        #Redirection page de sauvegarde depuis menu
        load_button = button_class(0.5, 0.7,
                                button_surfaces(button_surface(l=230),
                                                button_surface(l=230, outline_color=GREEN)
                                                ),
                                text = 'Sauvegardes',
                                action = lambda s: scene_manager.goto_scene('load') 

                                )

        #Mode d'emploi
        emploi_button =  button_class(0.9, 0.1,
                                button_surfaces(button_surface(l=230),
                                                button_surface(l=230, outline_color=GREEN)
                                                ),
                                text = "Mode d'emploi",
                                action = lambda s: scene_manager.goto_scene('mode_emploi') 

                                )
        
        emploi_text = text_label_class(0.5, 0.1, text='Bienvenue !', text_color=ALMOST_BLACK)
        emploi_text1 = text_label_class(0.5, 0.2, text='Ravi de faire votre connaissance !', text_color=ALMOST_BLACK)
        emploi_text2 = text_label_class(0.5, 0.3, text='Avant de commencer, veuillez selectionner le mode de jeu qui vous interesse', text_color=ALMOST_BLACK)

        #Mode d'emploi normal
        emploi_normal_button =  button_class(0.3, 0.5,
                                button_surfaces(button_surface(l=230),
                                                button_surface(l=230, outline_color=GREEN)
                                                ),
                                text = 'Normal',
                                action = lambda s: scene_manager.goto_scene('emploi_normal') 

                                )
        emploi_normal_text = text_label_class(0.5, 0.1, text='Le mode normal', text_color=ALMOST_BLACK)
        emploi_normal_text1 = text_label_class(0.5, 0.2, text='Essayez de trouver la bonne case !', text_color=ALMOST_BLACK)
        emploi_normal_text2 = text_label_class(0.5, 0.3, text='Vous pouvez poser des questions en selectionnant', text_color=ALMOST_BLACK)
        emploi_normal_text3 = text_label_class(0.5, 0.4, text='les categories et les différents attributs.', text_color=ALMOST_BLACK)
        emploi_normal_text4 = text_label_class(0.5, 0.5, text='Il est possible de poser des questions complexes "et", "ou".', text_color=ALMOST_BLACK)
        emploi_normal_text5 = text_label_class(0.5, 0.6, text='Vous obtiendrez une réponse positive ou négative.', text_color=ALMOST_BLACK)
        emploi_normal_text6 = text_label_class(0.5, 0.7, text="N'oubliez pas de cliquer sur le bouton valider, vous avez droit à 3 essais !", text_color=ALMOST_BLACK)
        emploi_normal_text7 = text_label_class(0.5, 0.8, text="Vous pouvez à tout moment sauvegarder ", text_color=ALMOST_BLACK)
        emploi_normal_text8 = text_label_class(0.55, 0.9, text="votre partie ou abandonner.", text_color=ALMOST_BLACK)
        
        emploi_triche_button =  button_class(0.5, 0.5,
                                button_surfaces(button_surface(l=230),
                                                button_surface(l=230, outline_color=GREEN)
                                                ),
                                text = 'Triche',
                                action = lambda s: scene_manager.goto_scene('emploi_triche') 

                                )
        emploi_triche_text = text_label_class(0.5, 0.1, text='Le mode triche', text_color=ALMOST_BLACK)
        emploi_triche_text1 = text_label_class(0.5, 0.2, text='Essayez de trouver la bonne case !', text_color=ALMOST_BLACK)
        emploi_triche_text2 = text_label_class(0.5, 0.3, text='La particularité de ce mode par rapport', text_color=ALMOST_BLACK)
        emploi_triche_text3 = text_label_class(0.5, 0.4, text='au mode normal est l\'apparition du bouton "estimer".', text_color=ALMOST_BLACK)
        emploi_triche_text4 = text_label_class(0.5, 0.5, text='Vous pouvez estimer le nombre de cases à éliminer.', text_color=ALMOST_BLACK)
        emploi_triche_text5 = text_label_class(0.5, 0.6, text='Après avoir estimé vous pouvez valider ou non votre question.', text_color=ALMOST_BLACK)
        emploi_triche_text6 = text_label_class(0.5, 0.7, text='Avec le bouton "demander" les cases sont automatiquement cochées', text_color=ALMOST_BLACK)
        emploi_triche_text7 = text_label_class(0.5, 0.8, text="Vous pouvez à tout moment sauvegarder", text_color=ALMOST_BLACK)
        emploi_triche_text8 = text_label_class(0.55, 0.9, text="votre partie ou abandonner.", text_color=ALMOST_BLACK)
        
        emploi_double_button =  button_class(0.7, 0.5,
                                button_surfaces(button_surface(l=230),
                                                button_surface(l=230, outline_color=GREEN)
                                                ),
                                text = 'Double',
                                action = lambda s: scene_manager.goto_scene('emploi_double') 

                                )
        emploi_double_text = text_label_class(0.5, 0.1, text='Le mode double', text_color=ALMOST_BLACK)
        emploi_double_text1 = text_label_class(0.5, 0.2, text='Essayez de trouver les bonnes cases !', text_color=ALMOST_BLACK)
        emploi_double_text2 = text_label_class(0.5, 0.3, text='Dans ce mode, le but est de trouver les deux cases auxquelles je pense !', text_color=ALMOST_BLACK)
        emploi_double_text3 = text_label_class(0.5, 0.4, text="Le mode de fonctionnement est le même que pour celui du mode normal", text_color=ALMOST_BLACK)
        emploi_double_text4 = text_label_class(0.5, 0.5, text='Vous pouvez rajouter des conditions aux questions :"au moins un",', text_color=ALMOST_BLACK)
        emploi_double_text5 = text_label_class(0.5, 0.6, text='"les deux","aucun", "un seul", "au plus un",', text_color=ALMOST_BLACK)
        emploi_double_text6 = text_label_class(0.5, 0.7, text='En validant, vous saurez le nombre de bonnes réponses !', text_color=ALMOST_BLACK)
        emploi_double_text7 = text_label_class(0.5, 0.8, text="Vous pouvez à tout moment sauvegarder", text_color=ALMOST_BLACK)
        emploi_double_text8 = text_label_class(0.55, 0.9, text="votre partie ou abandonner.", text_color=ALMOST_BLACK)

        emploi_facile_button =  button_class(0.5, 0.6,
                                button_surfaces(button_surface(l=230),
                                                button_surface(l=230, outline_color=GREEN)
                                                ),
                                text = 'Facile',
                                action = lambda s: scene_manager.goto_scene('emploi_facile') 

                                )
        emploi_facile_text = text_label_class(0.5, 0.1, text='Le mode facile', text_color=ALMOST_BLACK)
        emploi_facile_text1 = text_label_class(0.5, 0.2, text='Essayez de trouver la bonne case !', text_color=ALMOST_BLACK)
        emploi_facile_text2 = text_label_class(0.5, 0.3, text='Ce mode est plus facile car il vous suffira', text_color=ALMOST_BLACK)
        emploi_facile_text3 = text_label_class(0.5, 0.4, text='de cliquer sur le bouton "demander" pour que', text_color=ALMOST_BLACK)
        emploi_facile_text4 = text_label_class(0.5, 0.5, text='les cases qui correspondent à vos questions ', text_color=ALMOST_BLACK)
        emploi_facile_text5 = text_label_class(0.5, 0.6, text='soient automatiquement cochées.', text_color=ALMOST_BLACK)
        emploi_facile_text6 = text_label_class(0.5, 0.7, text='Vous pouvez à tout moment sauvegarder', text_color=ALMOST_BLACK)
        emploi_facile_text7 = text_label_class(0.5, 0.8, text="votre partie ou abandonner.", text_color=ALMOST_BLACK)
        
        
                

        #Page menu sauvegarde : retour menu

        menu_button = button_class(0.1, 0.9,
                                button_surfaces(button_surface(),
                                                button_surface(outline_color=GREEN)
                                                ),
                                text = 'Menu',
                                action = lambda s: scene_manager.goto_scene('main_menu')
                                )
        


         #Page jeu : Selection des questions 

        def select_categorie(s):
           
            response_text_element.text = ""
            if s.name == "select_category_button_1" : 
                if self.selected_category>=len(self.allCategories)-1:
                    self.selected_category=0
                    self.attByCat=self.grilleActuelle.getAllAttributsByCategorie(self.allCategories[self.selected_category])
                else: 
                    self.selected_category +=1 
                    
                self.attribut = 0
                self.attByCat=self.grilleActuelle.getAllAttributsByCategorie(self.allCategories[self.selected_category])
                select_attribut_button_1.text=self.attByCat[self.attribut]
                s.text = self.allCategories[self.selected_category]
            else : 

                if self.selected_category2>=len(self.allCategories)-1:
                    self.selected_category2=0
                    
                    self.attByCat2=self.grilleActuelle.getAllAttributsByCategorie(self.allCategories[self.selected_category2])
                else: 
                    self.selected_category2 +=1 
                    
                self.attribut2 = 0
                self.attByCat2=self.grilleActuelle.getAllAttributsByCategorie(self.allCategories[self.selected_category2])
                select_attribut_button_2.text=self.attByCat2[self.attribut2]
               
                s.text = self.allCategories[self.selected_category2]
        
       
        
        def select_attribute(s):
            
            
            #question_text_element.text=""
            response_text_element.text = ""
            if s.name == "select_attribut_button_1" : 
                if self.attribut>=len(self.attByCat)-1:
                    self.attribut=0
                else: 
                    self.attribut +=1 
               
                s.text = self.attByCat[self.attribut]
            else : 
                if self.attribut2>=len(self.attByCat2)-1:
                    self.attribut2=0
                else: 
                    self.attribut2 +=1 

                
                s.text = self.attByCat2[self.attribut2]

         
        select_category_button_1 = named_button(0.85, 0.2,
                                        button_surfaces(button_surface(l=320),
                                                        button_surface(l=320,outline_color=GREEN)
                                                        ),
                                        text = 'brun',
                                        action = select_categorie,
                                        name ="select_category_button_1"
                                        )

        
        select_attribut_button_1 = named_button(0.85, 0.3,
                                        button_surfaces(button_surface(l=320),
                                                        button_surface(l=320,outline_color=GREEN)
                                                        ),
                                        text = 'brun',
                                        action = select_attribute, 
                                        name ="select_attribut_button_1"
                                        )

            
        select_category_button_2 = named_button(0.85, 0.5,
                                                button_surfaces(button_surface(l=320),
                                                                button_surface(l=320,outline_color=GREEN)
                                                                ),
                                                text = 'cheveux',
                                                action = select_categorie,
                                                name = "select_category_button_2"
                                                )


        select_attribut_button_2 = named_button(0.85, 0.6,
                                        button_surfaces(button_surface(l=320),
                                                        button_surface(l=320,outline_color=GREEN)
                                                        ),
                                        text = 'brun',
                                        action = select_attribute,
                                        name = "select_attribut_button_2"
                                        )

        
        #Page jeu : Selection des connecteurs

        liste_connector = ['', 'et', 'ou']
        liste_double = ['au moins une case', "les deux cases", "aucune case", "une seule case", "au plus une case"]

        


        def change_connector(s):
            response_text_element.text=""
            if self.connector >= len(liste_connector)-1 :
                self.connector = 0
            else : self.connector +=1

            s.text = liste_connector[self.connector]

            if liste_connector[self.connector] == 'et' or liste_connector[self.connector] == 'ou' :
                if select_category_button_2 not in scenes[1].widgets or select_category_button_2 not in scenes[2].widgets :
                    scenes[1].widgets.append(select_category_button_2)
                    scenes[2].widgets.append(select_category_button_2)
                    scenes[3].widgets.append(select_category_button_2)
                    scenes[4].widgets.append(select_category_button_2)

                if select_attribut_button_2 not in scenes[1].widgets or select_attribut_button_2 not in scenes[2].widgets :
                    scenes[1].widgets.append(select_attribut_button_2)
                    scenes[2].widgets.append(select_attribut_button_2)
                    scenes[3].widgets.append(select_attribut_button_2)
                    scenes[4].widgets.append(select_attribut_button_2)
                
                if question_text_element_4 not in scenes[1].elements or question_text_element_4 not in scenes[2].elements :
                    scenes[1].elements.append(question_text_element_4)
                    scenes[2].elements.append(question_text_element_4)
                    scenes[3].elements.append(question_text_element_4)
                    scenes[4].elements.append(question_text_element_4)
                    

            else :
                if select_category_button_2 in scenes[1].widgets or select_category_button_2 in scenes[2].widgets :
                    scenes[1].widgets.remove(select_category_button_2)
                    scenes[2].widgets.remove(select_category_button_2)
                    scenes[3].widgets.remove(select_category_button_2)
                    scenes[4].widgets.remove(select_category_button_2)
                
                if select_attribut_button_2 in scenes[1].widgets or select_attribut_button_2 in scenes[2].widgets :
                    scenes[1].widgets.remove(select_attribut_button_2)
                    scenes[2].widgets.remove(select_attribut_button_2)
                    scenes[3].widgets.remove(select_attribut_button_2)
                    scenes[4].widgets.remove(select_attribut_button_2)

                if question_text_element_4 in scenes[1].elements or question_text_element_4 in scenes[2].elements :
                    scenes[1].elements.remove(question_text_element_4)
                    scenes[2].elements.remove(question_text_element_4)
                    scenes[3].elements.remove(question_text_element_4)
                    scenes[4].elements.remove(question_text_element_4)
            
            if (mode_button.name == 'triche') :
                scene_manager.goto_scene('game_triche')
            elif (mode_button.name == 'double') : 
                scene_manager.goto_scene('double')
            elif(mode_button.name == 'facile') : 
                scene_manager.goto_scene('facile')
            else :
                scene_manager.goto_scene('game')

            
        connector_button = button_class(0.85,0.4,
                                button_surfaces(button_surface(),
                                                button_surface(outline_color=GREEN)
                                                ),
                                         text = '',
                                        action = change_connector
                                        )

        def change_double(s) : 
            response_text_element.text=""
            if self.double >= len(liste_double)-1 :
                self.double = 0
            else : self.double +=1

            s.text = liste_double[self.double]

        double_button = named_button(0.85,0.1,
                                button_surfaces(button_surface(l=320),
                                                button_surface(outline_color=GREEN, l=320)
                                                ),
                                        text = liste_double[self.double],
                                        action = change_double
                                        )
        
        def mask_asked():
            for card in self.cards :
                for case in self.grilleActuelle.grille :
                    if case.nom == card.name :
                        if case.croix == True :
                            mask(card.cache)
       
        #Page jeu : Question posée 
        

        def validate_question(s) :
            category_1 = select_category_button_1.text
            parameter_1 = select_attribut_button_1.text
            connector = connector_button.text
            mode=mode_button.name
            if self.connector != 0 :
                category_2 = select_category_button_2.text
                parameter_2 = select_attribut_button_2.text
            else :
                category_2 = None
                parameter_2 = None
            
           
            if mode=="normal":
               
                if (self.grilleActuelle.estCeQue(category_1,category_2,parameter_1,parameter_2,connector)) :
                    response_text_element.text = 'Oui'
                else :
                    response_text_element.text = 'Non'
                    
            elif mode == "double" : 
                if (self.grilleActuelle.estCeQueExtension(category_1,category_2,parameter_1,parameter_2,connector, self.double)) :
                    response_text_element.text = 'Oui'
                else :
                    response_text_element.text = 'Non'
                
            else:
                self.grilleActuelle.tricheValider(category_1,category_2,parameter_1,parameter_2,connector)
                mask_asked()

        def estimer(s) : 
            category_1 = select_category_button_1.text
            parameter_1 = select_attribut_button_1.text
            connector = connector_button.text
            mode=mode_button.name
            if self.connector != 0 :
                category_2 = select_category_button_2.text
                parameter_2 = select_attribut_button_2.text
            else :
                category_2 = None
                parameter_2 = None
            
            estimate_text.text ='le nombre de case à éliminer est '+ str(self.grilleActuelle.tricheEstimer(category_1,category_2,parameter_1,parameter_2,connector))
            
            
            

        validate_question_button = button_class(0.9,0.8,
                                button_surfaces(button_surface(l=200),
                                                button_surface(l=200,outline_color=GREEN)
                                                ),
                                         text = 'Demander ?',
                                        action = validate_question
                                        )

        estimate_button = button_class(0.9,0.7,
                                button_surfaces(button_surface(l=200),
                                                button_surface(l=200,outline_color=GREEN)
                                                ),
                                         text = 'Estimer ?',
                                        action = estimer
                                        )
        
        question_text_element = text_label_class(0.79, 0.05, text='Votre', text_color=ALMOST_BLACK)
        question_text_element_2 = text_label_class(0.73, 0.1, text='', text_color=ALMOST_BLACK)
        grille_text_element = text_label_class(0.84, 0.1, text=liste_grilles[self.used_grille], text_color=ALMOST_BLACK)
        question_text_element_3 = text_label_class(0.67, 0.2, text='a-t-il', text_color=ALMOST_BLACK)
        question_text_element_4 = text_label_class(0.67, 0.5, text='des', text_color=ALMOST_BLACK)
        
        estimate_text = text_label_class(0.5, 0.8, text='', text_color=ALMOST_BLACK)
       

        def select_answer(s):

            if s.name == 'doubleAnswer' : 
                if self.selected_answer2 >= len(self.liste_answers)-1 :
                    self.selected_answer2 = 0
                else : self.selected_answer2 +=1
            
                s.text = self.liste_answers[self.selected_answer2].capitalize()
                response_text_element.text = ''
                
            else : 

                
                if self.selected_answer >= len(self.liste_answers)-1 :
                    self.selected_answer = 0
                else : self.selected_answer +=1
                
                s.text = self.liste_answers[self.selected_answer].capitalize()
                response_text_element.text = ''
            


            
        select_answer_button = named_button(0.7, 0.9,
                                button_surfaces(button_surface(l=230),
                                                button_surface(l=230,outline_color=GREEN)
                                                ),
                                text = "Réponse 1",
                                action = select_answer,
                                name = ''
                                )

        select_answer_button2 = named_button(0.5, 0.9,
                                button_surfaces(button_surface(l=230),
                                                button_surface(l=230,outline_color=GREEN)
                                                ),
                                text = "Réponse 2",
                                action = select_answer,
                                name = 'doubleAnswer'
                                )

        
       
        
        def send_answer(s):
            self.jeu.essai +=1
            if self.jeu.getEssai() == self.jeu.nbEssai : 
                annonce_text_element.text = "Ceci est le dernier essai auquel vous aurez droit"
            elif self.jeu.getEssai()>self.jeu.nbEssai : 
                giveup_text_element_3.text = "Vous avez atteint le maximum d'essai"
                scene_manager.goto_scene("give_up")
            
            if mode_button.name == 'double' : 
                if self.grilleActuelle.verifPersoDouble(self.liste_answers[self.selected_answer], self.liste_answers[self.selected_answer2]) == True : 
                     scene_manager.goto_scene("win")
                elif self.grilleActuelle.verifPersoDouble(self.liste_answers[self.selected_answer], self.liste_answers[self.selected_answer2]) == "1" : 
                     response_text_element.text = "Une des réponses est exacte" 
                else : 
                    response_text_element.text = "Aucun des deux" 
                    
                    
            elif self.grilleActuelle.verifPerso(self.liste_answers[self.selected_answer]) : 
                scene_manager.goto_scene("win")
            elif self.grilleActuelle.verifPerso(self.liste_answers[self.selected_answer]) == False:
                response_text_element.text = "Mauvaise réponse" 
        
        #texte pour la réponse finale
        annonce_text_element = text_label_class(0.4, 0.03, text='', text_color=ALMOST_BLACK)
        

        send_answer_button = button_class(0.9, 0.9,
                                button_surfaces(button_surface(l=200),
                                                button_surface(l=200,outline_color=GREEN)
                                                ),
                                text = 'Valider',
                                action = send_answer
                                )

        response_text_element = text_label_class(0.5, 0.83, text='', text_color=ALMOST_BLACK)
         
        

        #Page jeu : Sauvegarder la partie en cours
        def save_party(s) :
            
            if s.text == "Oui" : 
                if self.party_loaded != "" : 
                    
                    sauvegarde(mode_button.name, self.grilleActuelle, self.party_loaded)
                else : 
                    sauvegarde(mode_button.name, self.grilleActuelle, nomSauvegarde.text)

                load_button_1.text='Vide'
                load_button_2.text='Vide'
                load_button_3.text='Vide'
                load_button_4.text='Vide'
                load_button_5.text='Vide'
                nomSauvegarde.text = ""
                

                self.saves = getPath(resource_path("Saves"))
                load_save()
                
                
                scene_manager.goto_scene('main_menu')
            else : 
                
                if self.jeu.mode == "triche" : 
                    scene_manager.goto_scene('game_triche')
                elif self.jeu.mode == "double" : 
                    scene_manager.goto_scene('double')

                else : 

                    scene_manager.goto_scene('game')
                
        def access_save_page(s) : 
            
            if nbSauvegarde() == False and self.party_loaded not in getPath(resource_path("Saves")) : 
                
                save_text_element2.text = "Vous avez trop de sauvegardes !"
                save_text_element4.text = "Pour supprimer la plus ancienne cliquez Oui"
                save_text_element5.text = "Pour reprendre la partie cliquez Non"
            
            if self.party_loaded in getPath(resource_path("Saves")) : 
                nomSauvegarde.text =  self.party_loaded
                       
            scene_manager.goto_scene('save')
               
        save_party_button = button_class(0.3, 0.9,
                                button_surfaces(button_surface(l=230),
                                                button_surface(l=230,outline_color=GREEN)
                                                ),
                                text = 'Sauvegarder',
                                action = access_save_page
                                )
        
        #Page sauvegarde 

        save_text_element = text_label_class(0.5, 0.1, text='Sauvegarder ?', text_color=ALMOST_BLACK)
        
        save_text_element2 = text_label_class(0.5, 0.3, text='', text_color=ALMOST_BLACK)
        save_text_element4 = text_label_class(0.5, 0.4, text='', text_color=ALMOST_BLACK)
        save_text_element5 = text_label_class(0.5, 0.5, text='', text_color=ALMOST_BLACK)
        save_text_element3 = text_label_class(0.5, 0.6, text='Nom de la sauvegarde', text_color=ALMOST_BLACK)
        
       
        nomSauvegarde = text_input_box_class(0.5, 0.7,
                                       button_surfaces(button_surface(color=ALMOST_BLACK,l=256, h=32)), 
                                    
                                       )
        
        save_oui_button = button_class(0.4, 0.8,
                                button_surfaces(button_surface(),
                                                button_surface(outline_color=GREEN)
                                                ),
                                text = 'Oui',
                                action = save_party
                                )

        save_non_button = button_class(0.6, 0.8,
                                button_surfaces(button_surface(),
                                                button_surface(outline_color=GREEN)
                                                ),
                                text = 'Non',
                                action = save_party
                                )
        

        #Page jeu : Redirection sur la page abandon de la partie 
        giveup_button = button_class(0.1, 0.9,
                                button_surfaces(button_surface(l=230),
                                                button_surface(l=230,outline_color=GREEN)
                                                ),
                                text = 'Abandonner',
                                action = lambda s: scene_manager.goto_scene('give_up') 
                                )


        #Page abandon     

        giveup_text_element = text_label_class(0.5, 0.1, text='Vous avez abandonné !', text_color=ALMOST_BLACK)
        giveup_text_element_2 = text_label_class(0.4, 0.5, text='La réponse était : ', text_color=ALMOST_BLACK)
        giveup_text_element_3 = text_label_class(0.4, 0.6, text='', text_color=ALMOST_BLACK)
        answer_text_element = text_label_class(0.6, 0.5, text= '' , text_color=ALMOST_BLACK)
        answer_text_element2 = text_label_class(0.8, 0.5, text= '' , text_color=ALMOST_BLACK)

        continue_button = button_class(0.5, 0.8,
                                button_surfaces(button_surface(l=230),
                                                button_surface(l=230,outline_color=GREEN)
                                                ),
                                text = 'Continuer',
                                action = lambda s: scene_manager.goto_scene('main_menu')
                                )



        load_button_1 = button_class(0.5, 0.3,
                                button_surfaces(button_surface(),
                                                button_surface(outline_color=GREEN)
                                                ),
                                text = 'Vide',
                                action = start_game
                                )
        load_button_2 = button_class(0.5, 0.4,
                                button_surfaces(button_surface(),
                                                button_surface(outline_color=GREEN)
                                                ),
                                text = 'Vide',
                                action = start_game
                                )
        load_button_3 = button_class(0.5, 0.5,
                                button_surfaces(button_surface(),
                                                button_surface(outline_color=GREEN)
                                                ),
                                text = 'Vide',
                                action = start_game
                                )
        load_button_4 = button_class(0.5, 0.6,
                                button_surfaces(button_surface(),
                                                button_surface(outline_color=GREEN)
                                                ),
                                text = 'Vide',
                                action = start_game
                                )
        load_button_5 = button_class(0.5, 0.7,
                                button_surfaces(button_surface(), 
                                                button_surface(outline_color=GREEN)
                                                ),
                                text = 'Vide',
                                action = start_game
                                )
        
        buttons = [load_button_1, load_button_2, load_button_3, load_button_4, load_button_5]
        
        def load_save() :
            self.saves = getPath(resource_path("Saves"))
            if len(self.saves)<5 :
                nb_saves = len(self.saves)
            else :
                nb_saves = 5
            for i in range(nb_saves) :
                if buttons[i].text != self.saves[i] :
                    buttons[i].text = self.saves[i]
                

        #Page sauvegarde : retour menu

        menu_button = button_class(0.1, 0.9,
                                button_surfaces(button_surface(),
                                                button_surface(outline_color=GREEN)
                                                ),
                                text = 'menu',
                                action = lambda s: scene_manager.goto_scene('main_menu')
                                )
        
        select_save_text_element = text_label_class(0.5, 0.1, text='Choisissez la partie à charger', text_color=ALMOST_BLACK)

        #Page gagnant 
        win_text = text_label_class(0.5, 0.1, text='Vous avez gagné !!', text_color=ALMOST_BLACK)
        win_text2 = text_label_class(0.4, 0.5, text='La réponse est bien :  ', text_color=ALMOST_BLACK)


         
        
        #Toutes les scenes et leurs boutons attribuées
        scenes = (scene_class(name='main_menu',
                            widgets=[start_button,grille_button,mode_button,random_button,load_button, sound_button, emploi_button],
                            elements=[title_element, label_grille_element, label_mode_element]
                          ),
                scene_class(name='game',
                            widgets=[select_category_button_1, select_attribut_button_1,connector_button, validate_question_button, select_answer_button, send_answer_button, giveup_button, sound_button, save_party_button],
                            elements=[annonce_text_element,question_text_element,question_text_element_2,grille_text_element,question_text_element_3,response_text_element]
                          ),
                scene_class(name='game_triche',
                            widgets=[select_category_button_1, select_attribut_button_1,connector_button,validate_question_button,select_answer_button, send_answer_button, giveup_button, sound_button, save_party_button, estimate_button],
                            elements=[annonce_text_element,question_text_element,question_text_element_2,grille_text_element,question_text_element_3,response_text_element, estimate_text]
                          ),
                scene_class(name='double',
                            widgets=[select_category_button_1, select_attribut_button_1,connector_button, validate_question_button, select_answer_button,select_answer_button2, send_answer_button, giveup_button, sound_button, save_party_button, double_button],
                            elements=[annonce_text_element,question_text_element_3,response_text_element]
                          ),
                scene_class(name='facile',
                            widgets=[select_category_button_1, select_attribut_button_1,connector_button, validate_question_button, select_answer_button, send_answer_button, giveup_button, sound_button, save_party_button],
                            elements=[annonce_text_element,question_text_element,question_text_element_2,grille_text_element,question_text_element_3,response_text_element]
                          ),
                scene_class(name='give_up',
                            widgets=[continue_button, sound_button],
                            elements=[giveup_text_element, giveup_text_element_2, giveup_text_element_3, answer_text_element, answer_text_element2]
                          ),
                scene_class(name='mode_emploi',
                            widgets=[menu_button, sound_button, emploi_normal_button, emploi_triche_button, emploi_double_button, emploi_facile_button],
                            elements=[emploi_text, emploi_text1, emploi_text2]
                          ),
                scene_class(name='emploi_normal',
                            widgets=[menu_button, sound_button, emploi_button],
                            elements=[emploi_normal_text, emploi_normal_text1, emploi_normal_text2, emploi_normal_text3, emploi_normal_text4, emploi_normal_text5, emploi_normal_text6, emploi_normal_text7, emploi_normal_text8]
                          ),
                scene_class(name='emploi_triche',
                            widgets=[menu_button, sound_button, emploi_button],
                            elements=[emploi_triche_text, emploi_triche_text1, emploi_triche_text2, emploi_triche_text3, emploi_triche_text4, emploi_triche_text5, emploi_triche_text6, emploi_triche_text7, emploi_triche_text8]
                          ),
                scene_class(name='emploi_double',
                            widgets=[menu_button, sound_button, emploi_button],
                            elements=[emploi_double_text, emploi_double_text1, emploi_double_text2, emploi_double_text3, emploi_double_text4, emploi_double_text5, emploi_double_text6, emploi_double_text7, emploi_double_text8]
                          ),

                scene_class(name='emploi_facile',
                            widgets=[menu_button, sound_button, emploi_button],
                            elements=[emploi_facile_text, emploi_facile_text1, emploi_facile_text2, emploi_facile_text3, emploi_facile_text4, emploi_facile_text5, emploi_facile_text6, emploi_facile_text7]
                          ),

                scene_class(name='load',
                            widgets=[menu_button, load_button_1, load_button_2, load_button_3, load_button_4, load_button_5, sound_button],
                            elements=[select_save_text_element]
                          ), 
                scene_class(name='save',
                            widgets=[save_oui_button, save_non_button, nomSauvegarde], 
                            elements=[save_text_element, save_text_element2, save_text_element3,save_text_element4, save_text_element5] ),
                scene_class(name='win',
                            widgets=[continue_button], 
                            elements=[win_text, win_text2, answer_text_element, answer_text_element2] )
              )


        scene_manager = scene_manager_class(scenes)


        #Lancement de l'application 
        def motor() :
            pygame.init()
            clock = pygame.time.Clock()
            pygame.display.set_caption("Qui est-ce ?")
            bg_surface = background_surface(resource_path("Ressources"+ slash +"imageFond.png"))
            

            running = True

            while running :
                events = pygame.event.get()

                remaining_events = []
                for event in events:
                    if (event.type == pygame.QUIT):
                        running = False
                    else: 
                        if event.type == pygame.WINDOWRESIZED:
                            bg_surface = background_surface(resource_path("Ressources"+ slash +"imageFond.png"))
                            title_element.update_pos
                            label_grille_element.update_pos
                            label_mode_element.update_pos
                        remaining_events.append(event)

                
                load_save()

                # clear the screen before rendering :
                screen.blit(bg_surface, (0, 0))

                scene_manager.run(remaining_events)
                
                pygame.display.flip()
            return running
            pygame.quit()
        motor()
