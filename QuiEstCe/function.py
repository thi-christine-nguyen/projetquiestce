import json
import os
import sys


from Mode import *
from pathlib import Path

def resource_path(relative_path):
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")
        
    return os.path.join(base_path, relative_path)
    
def parseJson(file):
    
    openFile=open(file,"r")
    fileContent=openFile.read()
    file_python=json.loads(fileContent)
    
    return file_python

def nbSauvegarde() : 
    if len(getPath(resource_path("Saves"))) < 5 :
        return True
    else : 
        return False 


def sauvegarde(mode, grille, nomSauvegarde): 
    #créer un fichier json qui enregistre l'état courant


    if (nbSauvegarde()) : 
        #copie de la grille
        jsonCopie = dict(grille.fichierJSON)

        if "mode" not in jsonCopie.keys() : 
            jsonCopie["mode"] = mode

        
        if "caseChoisie" not in jsonCopie.keys() : 
            jsonCopie["caseChoisie"] = []
            for i in grille.caseChoisie : 
                jsonCopie["caseChoisie"].append(i.getNom())
           

        if "casesCochees" not in jsonCopie.keys() : 
            jsonCopie["casesCochees"] = []
            
            for i in grille.grille : 
                if i.croix == True : 
                    jsonCopie["casesCochees"].append(i.getNom())
        else : 
            
            for i in grille.grille : 
                if i.croix == True : 
                    if i.getNom() not in jsonCopie["casesCochees"] : 
                        jsonCopie["casesCochees"].append(i.getNom())


        #with va automatiquement fermer le fichier à la fin de son utilisation
        with open(resource_path("./Saves/" + nomSauvegarde + ".json"), 'w') as f:
            f.write(json.dumps(jsonCopie, sort_keys=True, indent=4, separators=(',', ': ')))

    else : 
        save = getPath(resource_path("Saves"))
        supSave(save[0])
        sauvegarde(mode, grille, nomSauvegarde)

 

def supSave(nom) : 
    os.remove(resource_path("./Saves/" + nom + ".json"))


    
                    

def reprendre(nomSave) : 
    #nomSave est le nom de la sauvegarde
    try : 
        nomSave = resource_path("Saves/" + nomSave + ".json")

        info = parseJson(nomSave)
        return nomSave, info["mode"]

    except FileNotFoundError : 
        print("Le fichier n'existe pas") 


#récupère tous les noms des fichiers dans le dossier save
def getPath(dir) : 
    
    dir = resource_path(dir)
   
    list_of_files = filter( lambda x: os.path.isfile(os.path.join(dir, x)),
                        os.listdir(dir) )

    #Liste par date de modification 
    list_of_files = sorted( list_of_files,
                        key = lambda x: os.path.getmtime(os.path.join(dir, x))
                        )
    tabNom = []
   
    for path in list_of_files : 
        tabNom.append(path.split('.')[0])

    return tabNom
            
def getNomMode() : 
    tab = [] 

    for mode in Mode :  
        m = str(mode)
        tab.append(m.split('.')[1]) 
    
    return tab




