import random
from Case import Case
from function import *

class Grille:

#constructeur
    def __init__(self, fichierJSON):
       
       
      
        #Si il manque le .json
        if fichierJSON[-5:] != ".json" :
            fichierJSON = fichierJSON+".json"

        try : 
            
            self.fichierJSON = parseJson(fichierJSON)
            self.type = self.fichierJSON["type"]
            self.nombreLignes = self.fichierJSON["ligne"]
            self.nombreColonnes = self.fichierJSON["colonne"]
            self.allPossibilites = self.fichierJSON["possibilites"]
            self.cles = list(self.allPossibilites.keys())
            self.grille=[]
            self.caseChoisie = []

            for i in self.cles:
                self.grille.append(Case(i, self.allPossibilites[i]["nom"], self.allPossibilites[i]))

            jsonKey = list(self.fichierJSON.keys())
            
            if "casesCochees" in jsonKey : 
                for i in self.fichierJSON["casesCochees"] : 
                    for j in self.grille : 
                        if i == j.getNom() : 
                            j.setCroix(True); 
            
            if "caseChoisie" in jsonKey : 
                for i in self.grille : 
                    for j in self.fichierJSON["caseChoisie"] : 
                        if i.getNom() == j : 
                            self.caseChoisie.append(i)
                
            else : 
                self.caseChoisie.append(random.choice(self.grille))

            
           

  
        except FileNotFoundError : 
            print("Le fichier", fichierJSON, " n'existe pas")


#getters
    def getType(self):
        return self.fichierJSON["type"]

    def getAllCategories(self):
        return self.fichierJSON["categories"]

    def getAllAttributsByCategorie(self, cat):
        return self.fichierJSON[cat]
    
    def getAllNoms(self): 
        return self.fichierJSON["noms"]

#setters 
    def addCaseChoisie(self) : 
        r = random.choice(self.grille)
        while r == self.caseChoisie[0] : 
            r = random.choice(self.grille)
                
        self.caseChoisie.append(r)

    



#fonctions
    def selectionCase(self, case):
        if case.croix == True : 
            case.croix = False

        else :
            case.croix = True


    def estCeQue(self, cat1, cat2, att1, att2, choix):
        if (choix == "ou"):
            return att1 in self.caseChoisie[0].tabAttribut[cat1] or att2 in self.caseChoisie[0].tabAttribut[cat2]
        elif (choix=="et"):
            return att1 in self.caseChoisie[0].tabAttribut[cat1] and att2 in self.caseChoisie[0].tabAttribut[cat2]
        else:
            return att1 in self.caseChoisie[0].tabAttribut[cat1]


    # mode Triche
    def tricheEstimer(self, cat1, cat2, att1, att2, choix):
        cpt=0
        li = list(self.allPossibilites.keys())
        flag=self.estCeQue(cat1,cat2,att1,att2,choix)
        if (choix == "ou"):
            if flag:
                for i in range(len(self.grille)):
                    if not(att1 in self.grille[i].tabAttribut[cat1] or att2 in self.grille[i].tabAttribut[cat2]):
                        cpt+=1
            else:
                for i in range(len(self.grille)):
                    if att1 in self.grille[i].tabAttribut[cat1] or att2 in self.grille[i].tabAttribut[cat2]:
                        cpt += 1
        elif (choix=="et"):
            if flag:
                for i in range(len(self.grille)):
                    if not(att1 in self.grille[i].tabAttribut[cat1] and att2 in self.grille[i].tabAttribut[cat2]):
                        cpt += 1
            else:
                for i in range(len(self.grille)):
                    if att1 in self.grille[i].tabAttribut[cat1] and att2 in self.grille[i].tabAttribut[cat2]:
                        cpt += 1
        else:
            if flag:
                for i in range(len(self.grille)):
                    if not (att1 in self.grille[i].tabAttribut[cat1]):
                        cpt += 1
                        # cpt est le nb de persos qui n'ont PAS l'attribut att1 pour la catégorie cat1
            else:
                for i in range(len(self.grille)):
                    if att1 in self.grille[i].tabAttribut[cat1]:
                        cpt += 1
                        # cpt  est le nb de persos qui ont l'attribut att1 pour la catégorie cat1
        return cpt


    def tricheValider(self, cat1, cat2, att1, att2, choix):
        
        flag=self.estCeQue(cat1,cat2,att1,att2,choix)
        if (choix == "ou"):
            if flag:
                for i in range(len(self.grille)):
                    if not(att1 in self.grille[i].tabAttribut[cat1] or att2 in self.grille[i].tabAttribut[cat2]):
                        self.grille[i].setCroix(True)
            else:
                for i in range(len(self.grille)):
                    if att1 in self.grille[i].tabAttribut[cat1] or att2 in self.grille[i].tabAttribut[cat2]:
                        self.grille[i].setCroix(True)
        elif (choix=="et"):
            if flag:
                for i in range(len(self.grille)):
                    if not(att1 in self.grille[i].tabAttribut[cat1] and att2 in self.grille[i].tabAttribut[cat2]):
                        self.grille[i].setCroix(True)
            else:
                for i in range(len(self.grille)):
                    if att1 in self.grille[i].tabAttribut[cat1] and att2 in self.grille[i].tabAttribut[cat2]:
                        self.grille[i].setCroix(True)
        else:
            if flag:
                for i in range(len(self.grille)):
                    if not (att1 in self.grille[i].tabAttribut[cat1]):
                        self.grille[i].setCroix(True)
                        # coche la case du perso s'il n'a PAS l'attribut att1 pour la catégorie cat1
            else:
                for i in range(len(self.grille)):
                    if att1 in self.grille[i].tabAttribut[cat1]:
                        self.grille[i].setCroix(True)
                        # coche la case du perso s'il a l'attribut att1 pour la catégorie cat1


    def verifPerso(self, perso): 
        if perso == self.caseChoisie[0].getNom():
            return True

        else:
            return False

    def verifPersoDouble(self, perso, perso2): 
        
        if (perso == self.caseChoisie[0].getNom() and perso2 == self.caseChoisie[1].getNom()) or (perso == self.caseChoisie[1].getNom() and perso2 == self.caseChoisie[0].getNom()): 
            return True
        elif (perso == self.caseChoisie[0].getNom() or perso2 == self.caseChoisie[1].getNom()) or (perso == self.caseChoisie[1].getNom() or perso2 == self.caseChoisie[0].getNom()): 
            return "1"
        else : 
            return False
       


    

    def estCeQueExtension(self, cat1, cat2, att1, att2, choix, type):
        # choix est "ou", "et" ou "", il permet de poser des questions complexes.
        # type = 0, 1, 2, 3, 4. Il est spécifique à l'extension avec 2 cases à trouver
        # 0 -> au moins une case
        # 1 -> les deux cases
        # 2 -> aucune case
        # 3 -> une seule case
        # 4 -> au plus une case

        res1 = False
        res2 = False
        if (choix == "ou"):
            res1 = att1 in self.caseChoisie[0].tabAttribut[cat1] or att2 in self.caseChoisie[0].tabAttribut[cat2]
            res2 = att1 in self.caseChoisie[1].tabAttribut[cat1] or att2 in self.caseChoisie[1].tabAttribut[cat2]

        elif (choix == "et"):
            res1 = att1 in self.caseChoisie[0].tabAttribut[cat1] and att2 in self.caseChoisie[0].tabAttribut[cat2]
            res2 = att1 in self.caseChoisie[1].tabAttribut[cat1] and att2 in self.caseChoisie[1].tabAttribut[cat2]

        else:
            res1 = att1 in self.caseChoisie[0].tabAttribut[cat1]
            res2 = att1 in self.caseChoisie[1].tabAttribut[cat1]

        if type == 0:
            return res1 or res2
        elif type == 1:
            return res1 and res2
        elif type == 2:
            return not (res1 or res2)
        elif type == 3:
            return not (res1 == res2)
        else:
            return not (res1 and res2)







