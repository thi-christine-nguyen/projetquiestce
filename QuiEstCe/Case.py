# classe Case

class Case():
    def __init__(self, idc, nom, att):
        self.idc = idc
        self.nom = nom
        self.tabAttribut = att
        self.cheminImage = att["fichier"]
        self.croix = False

# getters usuels
    def getId(self):
        return self.idc

    def getNom(self):
        return self.nom

    def getAllAttributs(self):
        return self.tabAttribut

    def getAttributByCategorie(self, cat):
        return self.tabAttribut[cat]

# gestion de l'attribut croix
    def getCroix(self):
        return self.croix

    def setCroix(self, boolean):
        self.croix = boolean







