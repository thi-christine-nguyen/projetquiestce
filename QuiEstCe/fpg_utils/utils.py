'''
summary :
# ~~ const ~~ :
    # colors
    WHITE
    LIGHT_GRAY
    GRAY
    DARK_GRAY
    ALMOST_BLACK
    BLACK
    RED
    GREEN
    BLUE
# ~~ random utility functions and classes ~~
    def in_rect
    def draw
    def aafilled_circle
    def ssaa_circle
    def ssaa_rect
    def button_surface
    CENTER
    LEFT
    def render_text

    def placed_pos

    # widgets
    class widgets_manager_class
    class button_surfaces
    class base_widget_interaction_class
    class base_widget_class
    class button_class
    class text_input_box_class

    # elements
    class text_label_class
    class fps_counter_class

    # scene stuff
    class scene_manager_class
    class scene_class
'''

from re import A
import pygame
from pygame.locals import SRCALPHA, RESIZABLE
pygame.init()
base_font = pygame.font.Font(None, 40)
button_font = pygame.font.SysFont(("Ink Free",
                                   "Segoe Script"
                                   "Segoe Print",
                                   "Comic Sans MS"),
                                   40, bold=False)
TOUCH = False
screen = pygame.display.set_mode((1260, 680), flags=RESIZABLE) # TODO replace by .get_screen()

# ~~ const ~~
# colors
WHITE = (255, 255, 255)
LIGHT_GRAY = (192, 192, 192)
GRAY = (128, 128, 128)
DARK_GRAY = (64, 64, 64)
ALMOST_BLACK = (32, 32, 32)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

# ~~ random utility functions and classes ~~
def in_rect(x, y, rx, ry, l, h):
    """ point(x, y)
        rect(rx, ry, l, h)
    """
    # not return (x >= rx) and (x <= rx + l) and (y >= ry) and (y <= ry + h)
    # because of [0:] [1:] shenanigans with px arrays, would make sense geometrically otherwise
    return (x >= rx) and (x < rx + l) and (y >= ry) and (y < ry + h)

def draw(surface, target_surface, x, y, alpha_target=None):
    """ pygame.draw methods overwrite the target surface on the relevant pixels,
        unlike blit, which blend them (no matter the special_flag).
        This function imitate pygame.draw methods on this, but for surface.
        This is pretty meaningful when surface with alpha are concerned, otherwise juste
        use blit, it will be much more efficient.
        Hence, surface is expected to be a RGBA pygame surface.

        If alpha_target is a valid alpha value, pixels from surface verifying 0 < alpha < alpha_target
        will be merged with pixels from target surface, while pixels with alpha >= alpha_target will
        overwrite as usual the pixels from target_surface.
        It's useful for example if surface is a image with global transparency, but also as aa and you
        want it to behave as you'd expect, merging with the bg to make the outline smooth.
    """
    l, h = surface.get_size()
    if alpha_target is None:
        for px_x in range(l):
            for px_y in range(h):
                color = surface.get_at((px_x, px_y))
                if color[3] > 0: # if the pixel isn't completely transparent
                    target_surface.set_at((x + px_x, y + px_y), color)
    else:
        for px_x in range(l):
            for px_y in range(h):
                target_px_coord = (x + px_x, y + px_y)
                color = surface.get_at((px_x, px_y))
                alpha = color[3]
                if alpha > 0: # if the pixel isn't completely transparent
                    if alpha < alpha_target:
                        target_px_color = target_surface.get_at(target_px_coord)
                        target_surface.set_at(target_px_coord,
                                              (round((target_px_color[0] + color[0]) / 2),
                                               round((target_px_color[1] + color[1]) / 2),
                                               round((target_px_color[2] + color[2]) / 2),
                                               round((target_px_color[3] + color[3]) / 2)
                                               )
                                              )
                    else: target_surface.set_at(target_px_coord, color)

def ssaa_circle(target_surface, color, center, radius, width=0,
                draw_top_right=False, draw_top_left=False, draw_bottom_left=False, draw_bottom_right=False):
    """ the same as pygame.draw.circle, but anti aliased through 2x super sampling
        follow pygame.draw.circle format for args
        Note, unlike pygame.draw.circle, width > 1 extend inward, hence a ssaa_circle will always
        fit inside the expected bounding_box.
    """
    diameter = radius + radius
    ss_size = diameter + diameter
    surface_to_shrink = pygame.Surface((ss_size, ss_size), flags=SRCALPHA)

    # otherwise with default black bg, smoothscale makes a dark outline when merging pixels
    #if len(color) == 3: surface_to_shrink.fill((*color, 0))
    #elif len(color) == 4: surface_to_shrink.fill((*color[0:3], 0))
    #else: raise ValueError("Expected a iterable with RGB, or RGBA format for color.")
    bg_color = (*color[0:3], 0)
    surface_to_shrink.fill(bg_color)

    if width > 1:
        pygame.draw.circle(surface_to_shrink, color, (diameter, diameter), diameter, 0,
                           draw_top_right, draw_top_left, draw_bottom_left, draw_bottom_right)
        pygame.draw.circle(surface_to_shrink, bg_color, (diameter, diameter), diameter - (width * 2), 0,
                           draw_top_right, draw_top_left, draw_bottom_left, draw_bottom_right)
    else:
        pygame.draw.circle(surface_to_shrink, color, (diameter, diameter), diameter, width,
                           draw_top_right, draw_top_left, draw_bottom_left, draw_bottom_right)

    x, y = center
    if len(color)>3:
        # if alpha, to mimic the overwriting behavior of pygame.draw methods,
        # despite being slower, draw() is needed instead of blit
        draw(pygame.transform.smoothscale(surface_to_shrink, (diameter, diameter)), target_surface, x - radius, y - radius, color[3])
    else:
        target_surface.blit(pygame.transform.smoothscale(surface_to_shrink, (diameter, diameter)), (x - radius, y - radius))

def ssaa_rect(target_surface, color, rect, width=0, border_radius=1):
    """ pygame.draw.rect with a border_radius != 0, but with 2x ssaa,
        Note, if you don't want rounded corners, there is no point in using this compared to
        pygame.draw.rect, hence why this function assume a border radius.
        Note (bis), unlike pygame.draw.rect, width > 1 extend inward, hence a ssaa_rect will always
        fit inside the expected bounding_box.
        follow pygame.draw.rect format for args
        TODO : add the possibility of having different radius or each corner
    """
    x, y, l, h = rect
    # it's not to opti by drawing only the necessary components (since it's actually slower), but to avoid
    # the issue when color as alpha and radius is double l or h, lines appear because of overdrawing.
    radius_is_double_l = (border_radius * 2 == l)
    radius_is_double_h = (border_radius * 2 == h)
    left_center_x = x + border_radius
    right_center_x = x + l - border_radius
    top_center_y = y + border_radius
    bottom_center_y = y + h - border_radius
    central_rect_l = right_center_x - left_center_x
    central_rect_h = bottom_center_y - top_center_y

    if width > 1:
        radius_minus_width = border_radius - width
        # top rect
        if not radius_is_double_l:
            pygame.draw.rect(target_surface, color, (left_center_x, y, central_rect_l, width))
        # right rect
        if not radius_is_double_h:
            pygame.draw.rect(target_surface, color, (right_center_x + radius_minus_width, top_center_y, width, central_rect_h))
        # bottom rect
        if not radius_is_double_l:
            pygame.draw.rect(target_surface, color, (left_center_x, bottom_center_y + radius_minus_width, central_rect_l, width))
        # left rect
        if not radius_is_double_h:
            pygame.draw.rect(target_surface, color, (x, top_center_y, width, central_rect_h))
    else:
        # central rect
        if not (radius_is_double_h or radius_is_double_l):
            pygame.draw.rect(target_surface, color, (left_center_x, top_center_y, central_rect_l, central_rect_h))
        # top rect
        if not radius_is_double_l:
            pygame.draw.rect(target_surface, color, (left_center_x, y, central_rect_l, border_radius))
        # right rect
        if not radius_is_double_h:
            pygame.draw.rect(target_surface, color, (right_center_x, top_center_y, border_radius, central_rect_h))
        # bottom rect
        if not radius_is_double_l:
            pygame.draw.rect(target_surface, color, (left_center_x, bottom_center_y, central_rect_l, border_radius))
        # left rect
        if not radius_is_double_h:
            pygame.draw.rect(target_surface, color, (x, top_center_y, border_radius, central_rect_h))

    # top left disc
    #aafilled_circle(target_surface, left_center_x, top_center_y, border_radius, color)
    ssaa_circle(target_surface, color, (left_center_x, top_center_y), border_radius, width, draw_top_left=True)
    # top right disc
    #aafilled_circle(target_surface, right_center_x, top_center_y, border_radius, color)
    ssaa_circle(target_surface, color, (right_center_x, top_center_y), border_radius, width, draw_top_right=True)
    # bottom left disc
    #aafilled_circle(target_surface, left_center_x, bottom_center_y, border_radius, color)
    ssaa_circle(target_surface, color, (left_center_x, bottom_center_y), border_radius, width, draw_bottom_left=True)
    # bottom right disc
    #aafilled_circle(target_surface, right_center_x, bottom_center_y, border_radius, color)
    ssaa_circle(target_surface, color, (right_center_x, bottom_center_y), border_radius, width, draw_bottom_right=True)

def button_surface(color=(128, 128, 128), l=128, h=64, border_radius=8,
                   outline_width=2, outline_color=(192, 192, 192)):
    """ return a button looking surface
    """
    surface = pygame.Surface((l, h), flags=SRCALPHA)
    ssaa_rect(surface, color, (0, 0, l, h), 0, border_radius)
    if outline_width > 0: ssaa_rect(surface, outline_color, (0, 0, l, h), outline_width, border_radius)
    return surface

def background_surface(image_path) :
    image = pygame.image.load(image_path).convert_alpha()
    return pygame.transform.smoothscale(image, (screen.get_width(), screen.get_height()))
    
CENTER = 10
LEFT = 14

def render_text(text='', text_color=(255, 255, 255), text_font=base_font,
                alignment=CENTER):
    """ return a surface of rendered text supporting a subset of markdown (discord) :
        - line break, \n
        - Bold, **text** # TODO
        - italic, *text* # TODO
        - underlined, __text__ # TODO
        as well as alignment options
    """
    # render each line of text to a surface and put them in a ordered list :
    rendered_lines = [text_font.render(line, True, text_color) for line in text.split("\n")]
    # prepare the final surface :
    max_l = 0
    total_h = 0
    for rendered_line in rendered_lines:
        l = rendered_line.get_width()
        if l > max_l: max_l = l
        total_h += rendered_line.get_height()
    rendered_text = pygame.Surface((max_l, total_h), flags=SRCALPHA)
    # blit the rendered lines of text onto the final surface according to alignment :
    line_y = 0
    if alignment == CENTER:
        for rendered_line in rendered_lines:
            rendered_text.blit(rendered_line, (round((max_l - rendered_line.get_width()) / 2), line_y))
            line_y += rendered_line.get_height()
    elif alignment == LEFT:
        for rendered_line in rendered_lines:
            rendered_text.blit(rendered_line, (0, line_y))
            line_y += rendered_line.get_height()
    else: raise ValueError("Expected CENTER or LEFT values for alignment option, got {alignment}")
    return rendered_text

class placed_surface(pygame.Surface):
    def __init__(self, *args):
        pygame.Surface.__init__(*args)
        self.x = 0
        self.y = 0


def placed_pos(target_surface: pygame.Surface,
               surface: pygame.Surface,
               pos_x: (int, float) = 0.5,
               pos_y: (int, float) = 0.5):
    """ return the position in target_surface space to place
        surface according to pos_x, pos_y from it's center

        pos_x : the x position of the button fom it's center
                if x a int, position in pixels
                if x a float <= 1, position as a fraction of the screen width
        pos_y : the y position of the button fom it's center
                if y a int, position in pixels
                if y a float <= 1, position as a fraction of the screen height
    """
    if isinstance(pos_x, int):
        surface_x = round(pos_x - surface.get_width() / 2)
    elif isinstance(pos_x, float):
        surface_x = round(target_surface.get_width() * pos_x - surface.get_width() / 2)
    else: raise ValueError('Expected int or float for pos_x.')
    if isinstance(pos_y, int):
        surface_y = round(pos_y - surface.get_height() / 2)
    elif isinstance(pos_y, float):
        surface_y = round(target_surface.get_height() * pos_y - surface.get_height() / 2)
    else: raise ValueError('Expected int or float for pos_y.')
    return surface_x, surface_y


# widgets
# TODO : widget base class where :
#        .surfaces is a named tuple (.surfaces.pressed, .hover, .base,...)
#        .update_pose iter the .surfaces named tuple so it works for any config of surfaces
#        streamline update pose intricate to a single func with place stuff
# TODO 2 : the EQUALIZE keyword for x_pos and y_pos

class widgets_manager_class():
    """ widgets are rendered directly onto the screen surface, not the game_view surface

        note : screen_space is necessary to have coord of events valid, hence target_surface is
               not changeable (yet, but I don't think it's possible to get the position of arbitrary
               surfaces to transform the coords without a custom surface class and a complete break from
               classic pygame compatibility)
    """
    def __init__(self):
        #self.target_surface = pygame.display.get_surface()
        self.active_widgets = []

    #def set_target_surface(self, target_surface):
    #    self.target_surface = target_surface

    def process_events(self, events):
        """
        for event in events:
            if event in self.event_sub_widgets_dict:
                widgets = self.event_sub_widgets_dict[event]
                for widget in widgets :
                    widget... ? todo and decide
        """
        for event in events:
            # TODO : ignore mouse events if fingers ones, as they would be duplicate
            # We look for finger down, finger motion, and then finger up.

            for widget in self.active_widgets:
                widget(event) # sigh...

    #def handle_event(self, fingerup):
    #    """ is called if a fingerup event is detected.
    #    """
    #    for widget in self.active_widgets:
    #        if widget.collide(fingerup.x, fingerup.y): pass

    def render(self):
        for widget in self.active_widgets: widget.render()


class button_surfaces():
    """ surface : base surface used to display the button
        hover_surface : surface displayed when the mouse is over the button,
                        or the button is selected with keyboard/gamepad
        pressed_surface : surface displayed when button is pressed
    """
    def __init__(self, surface: pygame.Surface,
                       hover_surface: pygame.Surface = None,
                       pressed_surface: pygame.Surface = None):
        self.surface = surface
        if hover_surface is None: self.hover_surface = surface
        else: self.hover_surface = hover_surface
        if pressed_surface is None: self.pressed_surface = surface
        else: self.pressed_surface = pressed_surface

class base_widget_interaction_class():
    def __init__(self):
        pass

    def on_mousemotion(self, event): pass

    def on_mousebuttonup(self, event): pass

    def on_mousebuttondown(self, event): pass

    def on_keyup(self, event): pass

    def on_keydown(self, event): pass

    def on_textinput(self, event): pass

    def __call__(self, event):
        """
        """
        if (not TOUCH) and (event.type == pygame.MOUSEMOTION) and (not event.touch):
            self.on_mousemotion(event)

        elif (not TOUCH) and (event.type == pygame.MOUSEBUTTONUP) and (not event.touch):
            self.on_mousebuttonup(event)

        elif (not TOUCH) and (event.type == pygame.MOUSEBUTTONDOWN) and (not event.touch):
            self.on_mousebuttondown(event)

        elif event.type == pygame.FINGERMOTION:
            if hasattr(self, 'on_fingermotion'): self.on_fingermotion(event)
            else: self.on_mousemotion(event)

        elif event.type == pygame.FINGERUP:
            if hasattr(self, 'on_fingerup'): self.on_fingerup(event)
            else: self.on_mousebuttonup(event)

        elif event.type == pygame.FINGERDOWN:
            if hasattr(self, 'on_fingerdown'): self.on_fingerdown(event)
            else: self.on_mousebuttondown(event)

        elif event.type == pygame.KEYUP:
            self.on_keyup(event)

        elif event.type == pygame.KEYDOWN:
            self.on_keydown(event)

        elif event.type == pygame.TEXTINPUT:
            self.on_textinput(event)

        elif event.type == pygame.WINDOWRESIZED:
            self.update_pos()

class base_displayable_widget_class():
    def __init__(self, x_pos, y_pos, surfaces):
        """ x_pos : the x position of the button fom it's center
                    if x a int, position in pixels
                    if x a float <= 1, position as a fraction of the screen width
            y_pos : the y position of the button fom it's center
                    if y a int, position in pixels
                    if y a float <= 1, position as a fraction of the screen height
            surfaces : a list/tuple/namedtuple of surfaces, the first is assumed as the default active

            active_surface : the surface to render
            active_surface_x : the x pos in px of the surface to render
            active_surface_y : the y pos in px of the surface to render
        """
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.target_surface = pygame.display.get_surface()
        self.surfaces = surfaces

        self.active_surface = self.surfaces[0]
        self.active_surface_x = 0
        self.active_surface_y = 0
        self.update_pos()

    def update_pos(self):
        """ will update the x, and y coord of where the active surface should be blit
            based on the coord of the button, and if relevant the target_surface size.
        """
        self.active_surface_x, self.active_surface_y = placed_pos(self.target_surface, self.active_surface,
                                                                  self.x_pos, self.y_pos)

    def point_on_widget(self, x, y):
        """ the active surface can be anything in shape,
            so we first check if the point is on the surface rect,
            then if the pixel at those coord of the surface is not empty (alpha != 0)
        """
        return in_rect(x, y, self.active_surface_x, self.active_surface_y, *self.active_surface.get_size()) and\
               (self.active_surface.get_at((x - self.active_surface_x, y - self.active_surface_y))[3] != 0)

    def render(self):
        """
        """
        self.target_surface.blit(self.active_surface, (self.active_surface_x, self.active_surface_y))

    def on_mousemotion(self, event): pass

    def on_mousebuttonup(self, event): pass

    def on_mousebuttondown(self, event): pass

    def on_keyup(self, event): pass

    def on_keydown(self, event): pass

    def on_textinput(self, event): pass

    def __call__(self, event):
        """
        """
        if event.type == pygame.MOUSEMOTION and not event.touch:
            self.on_mousemotion(event)

        elif event.type == pygame.MOUSEBUTTONUP and not event.touch:
            self.on_mousebuttonup(event)

        elif event.type == pygame.MOUSEBUTTONDOWN and not event.touch:
            self.on_mousebuttondown(event)

        elif event.type == pygame.FINGERMOTION:
            if hasattr(self, 'on_fingermotion'): self.on_fingermotion(event)
            else: self.on_mousemotion(event)

        elif event.type == pygame.FINGERUP:
            if hasattr(self, 'on_fingerup'): self.on_fingerup(event)
            else: self.on_mousebuttonup(event)

        elif event.type == pygame.FINGERDOWN:
            if hasattr(self, 'on_fingerdown'): self.on_fingerdown(event)
            else: self.on_mousebuttondown(event)

        elif event.type == pygame.KEYUP:
            self.on_keyup(event)

        elif event.type == pygame.KEYDOWN:
            self.on_keydown(event)

        elif event.type == pygame.TEXTINPUT:
            self.on_textinput(event)

        elif event.type == pygame.WINDOWRESIZED:
            self.update_pos()


class button_class(base_widget_interaction_class):
    def __init__(self, x_pos, y_pos,
                 surfaces: button_surfaces,
                 text=None, text_color=(255, 255, 255), text_font=button_font,
                 action=lambda s: print("Button was clicked."),
                 hover_cursor = pygame.cursors.Cursor(pygame.SYSTEM_CURSOR_HAND),
                 action_on_pressed=False):
        """ A basic button, customizable through the surfaces parameter.
            Do handle dynamically changing the text parameter, if it was not initialised with None.

            x_pos : the x position of the button fom it's center
                    if x a int, position in pixels
                    if x a float <= 1, position as a fraction of the screen width
            y_pos : the y position of the button fom it's center
                    if y a int, position in pixels
                    if y a float <= 1, position as a fraction of the screen height
            surfaces : a button_surfaces instance, see it's doc for supported surfaces
            text : if not None, will be rendered line by line, centered on the button
            text_color : for the text color
            text font : for the text font
            action : the function that will be called when the button is pressed
            hover_cursor : a pygame.Cursor of what the cursor should be when the mouse hover the button
            action_on_pressed : if True, action is called when the button is pressed, not released.
                                It is recommended to use False for UI button, as it allow to rectify miss-clicks
                                for the user, and to use True for gameplay button, so they react instantly.

            active_surface : the surface to render
            active_surface_h : the height of the surface to render
            active_surface_l : the length of the surface to render
            active_surface_x : the x pos in px of the surface to render
            active_surface_y : the y pos in px of the surface to render

            in_focus : a bool, self-descriptive
        """
       
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.target_surface = pygame.display.get_surface()
        self.surfaces = surfaces
        self.text = text
        self.text_color = text_color
        self.text_font = text_font
        self.action = action
        self.hover_cursor = hover_cursor
        self.action_on_pressed = action_on_pressed

        if not (text is None):
            self.rendered_text = render_text(text, text_color, text_font)
            self.previous_text = self.text

        self.active_surface = self.surfaces.surface
        self.active_surface_l, self.active_surface_h = self.active_surface.get_size()
        self.active_surface_x = 0
        self.active_surface_y = 0
        self.update_pos()

        self.in_focus = False

        # TODO :
        #   maybe at init of the generic displayable widget, check which event method are available
        #   and adapt __call__ in function ?
        #   need a way to be extendable for children/maintainability of having the __call__ mostly everywhere
        #   just the specific on_event method changing

    def update_pos(self):
        """ will update the x, and y coord of where the active surface should be blit
            based on the coord of the button, and if relevant the target_surface size.
        """
        self.active_surface_l, self.active_surface_h = self.active_surface.get_size()
        self.active_surface_x, self.active_surface_y = placed_pos(self.target_surface, self.active_surface, self.x_pos, self.y_pos)
        if self.text is not None:
            self.rendered_text_x, self.rendered_text_y = placed_pos(self.active_surface, self.rendered_text)
            self.rendered_text_x += self.active_surface_x
            self.rendered_text_y += self.active_surface_y

    def point_on_widget(self, x, y):
        """ the active surface can be anything in shape,
            so we first check if the point is on the surface rect,
            then if the pixel at those coord of the surface is not empty (alpha != 0)
        """
        if in_rect(x, y,
                   self.active_surface_x, self.active_surface_y,
                   self.active_surface_l, self.active_surface_h):
            return self.active_surface.get_at((x - self.active_surface_x, y - self.active_surface_y))[3] != 0
        return False

    def render(self):
        """
        """
        self.target_surface.blit(self.active_surface, (self.active_surface_x, self.active_surface_y))
        if not (self.text is None):
            # surprisingly efficient no matter the string size, since python compare the hash
            if self.text != self.previous_text:
                self.rendered_text = render_text(self.text, self.text_color, self.text_font)
                self.update_pos()
                self.previous_text = self.text
            self.target_surface.blit(self.rendered_text, (self.rendered_text_x, self.rendered_text_y))

    def on_mousemotion(self, event):
        mouse_x, mouse_y = pygame.mouse.get_pos()
        if self.point_on_widget(mouse_x, mouse_y):
            if not self.in_focus:
                self.active_surface = self.surfaces.hover_surface
                if not (self.hover_cursor is None):
                    pygame.mouse.set_cursor(self.hover_cursor)
                self.in_focus = True
        else:
            if self.in_focus:
                self.active_surface = self.surfaces.surface
                if not (self.hover_cursor is None):
                    pygame.mouse.set_cursor(pygame.cursors.Cursor(pygame.SYSTEM_CURSOR_ARROW))
                self.in_focus = False

    def on_mousebuttondown(self, event):
        if self.action_on_pressed:
            mouse_x, mouse_y = pygame.mouse.get_pos()
            if self.point_on_widget(mouse_x, mouse_y):
                self.action(self)

    def on_mousebuttonup(self, event):
        if not self.action_on_pressed:
            mouse_x, mouse_y = pygame.mouse.get_pos()
            if self.point_on_widget(mouse_x, mouse_y):
                self.action(self)

    def on_fingermotion(self, event):
        # defined because the default of using on_mousemotion would crash
        pass

    def on_fingerdown(self, event):
        if self.action_on_pressed:
            finger_x, finger_y = event.x, event.y
            if self.point_on_widget(finger_x, finger_y):
                self.action(self)

    def on_fingerup(self, event):
        if not self.action_on_pressed:
            finger_x, finger_y = event.x, event.y
            if self.point_on_widget(finger_x, finger_y):
                self.action(self)


class text_input_box_class(base_widget_interaction_class):
    # TODO
    """ when clicked, launch the text input event system, then write in it what's inputed
        can have text size limit
        when click outside -> deactivate text_input event system

        TODO : to use for default hover_cursor : pygame.SYSTEM_CURSOR_IBEAM
    """
    def __init__(self, x_pos, y_pos,
                 surfaces: button_surfaces,
                 text='', text_color=(255, 255, 255), text_font=base_font,
                 hover_cursor = pygame.cursors.Cursor(pygame.SYSTEM_CURSOR_IBEAM)):
        """ A basic button, customizable through the surfaces parameter.
            Do handle dynamically changing the text parameter, if it was not initialised with None.

            x_pos : the x position of the button fom it's center
                    if x a int, position in pixels
                    if x a float <= 1, position as a fraction of the screen width
            y_pos : the y position of the button fom it's center
                    if y a int, position in pixels
                    if y a float <= 1, position as a fraction of the screen height
            surfaces : a button_surfaces instance, see it's doc for supported surfaces
            text : if not None, will be rendered line by line, centered on the button
            text_color : for the text color
            text font : for the text font.

            active_surface : the surface to render
            active_surface_h : the height of the surface to render
            active_surface_l : the length of the surface to render
            active_surface_x : the x pos in px of the surface to render
            active_surface_y : the y pos in px of the surface to render

            in_focus : a bool, self-descriptive
        """
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.target_surface = pygame.display.get_surface()
        self.surfaces = surfaces
        self.text = text
        self.text_color = text_color
        self.text_font = text_font
        self.hover_cursor = hover_cursor

        self.rendered_text = render_text(text, text_color, text_font)
        self.previous_text = self.text

        self.active_surface = self.surfaces.surface
        self.active_surface_l, self.active_surface_h = self.active_surface.get_size()
        self.active_surface_x = 0
        self.active_surface_y = 0
        self.update_pos()

        self.in_focus = False
        self.take_input = False

        # TODO :
        #   maybe at init of the generic displayable widget, check which event method are available
        #   and adapt __call__ in function ?
        #   need a way to be extendable for children/maintainability of having the __call__ mostly everywhere
        #   just the specific on_event method changing

    def update_pos(self):
        """ will update the x, and y coord of where the active surface should be blit
            based on the coord of the button, and if relevant the target_surface size.
        """
        self.active_surface_l, self.active_surface_h = self.active_surface.get_size()
        self.active_surface_x, self.active_surface_y = placed_pos(self.target_surface, self.active_surface, self.x_pos, self.y_pos)
        if self.text is not None:
            self.rendered_text_x, self.rendered_text_y = placed_pos(self.active_surface, self.rendered_text)
            self.rendered_text_x += self.active_surface_x
            self.rendered_text_y += self.active_surface_y

    def point_on_widget(self, x, y):
        """ the active surface can be anything in shape,
            so we first check if the point is on the surface rect,
            then if the pixel at those coord of the surface is not empty (alpha != 0)
        """
        if in_rect(x, y,
                   self.active_surface_x, self.active_surface_y,
                   self.active_surface_l, self.active_surface_h):
            return self.active_surface.get_at((x - self.active_surface_x, y - self.active_surface_y))[3] != 0
        return False

    def render(self):
        """
        """
        self.target_surface.blit(self.active_surface, (self.active_surface_x, self.active_surface_y))
        if not (self.text is None):
            # surprisingly efficient no matter the string size, since python compare the hash
            if self.text != self.previous_text:
                self.rendered_text = render_text(self.text, self.text_color, self.text_font)
                self.update_pos()
                self.previous_text = self.text
            self.target_surface.blit(self.rendered_text, (self.rendered_text_x, self.rendered_text_y))

    def on_mousemotion(self, event):
        mouse_x, mouse_y = pygame.mouse.get_pos()
        if self.point_on_widget(mouse_x, mouse_y):
            if not self.in_focus:
                self.active_surface = self.surfaces.hover_surface
                if not (self.hover_cursor is None):
                    pygame.mouse.set_cursor(self.hover_cursor)
                self.in_focus = True
        else:
            if self.in_focus:
                self.active_surface = self.surfaces.surface
                if not (self.hover_cursor is None):
                    pygame.mouse.set_cursor(pygame.cursors.Cursor(pygame.SYSTEM_CURSOR_ARROW))
                self.in_focus = False

    def on_mousebuttonup(self, event):
        mouse_x, mouse_y = pygame.mouse.get_pos()
        if self.point_on_widget(mouse_x, mouse_y):
            self.take_input = True
            pygame.key.start_text_input()
        else:
            self.take_input = False
            pygame.key.stop_text_input()

    def on_fingermotion(self, event):
        # defined because the default of using on_mousemotion would crash
        pass

    def on_fingerup(self, event):
        finger_x, finger_y = event.x, event.y
        if self.point_on_widget(finger_x, finger_y):
            self.take_input = True
            pygame.key.start_text_input()
        else:
            self.take_input = False
            pygame.key.stop_text_input()

    def on_textinput(self, event):
        if self.take_input:
            self.text += event.text

    def on_keydown(self, event):
        if self.take_input:
            if event.key == pygame.K_BACKSPACE:
                if len(self.text) >= 1: self.text = self.text[0:-1]
            elif event.key == pygame.K_RETURN:
                self.take_input = False
                pygame.key.stop_text_input()


# elements :
class text_label_class():
    """ A element displaying text, with nothing else.
        To display text in a box, see text_input_box_class.
        Do handle dynamically changing the text parameter.
    """
    def __init__(self, x_pos, y_pos,
                 text='', text_color=(255, 255, 255), text_font=button_font):
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.target_surface = pygame.display.get_surface()

        self.text = text
        self.previous_text = text
        self.text_color = text_color
        self.text_font = text_font
        self.rendered_text = render_text(text, text_color, text_font)
        self.update_pos()

    def update_pos(self):
        """ will update the x, and y coord where the surface should be blit
            based on the coord of the label, and if relevant the target_surface size.

            should be called by the widget_manager when a window_resize event is caught,
            or by the .render method if .text is detected to have changed.
        """
        self.rendered_text_x, self.rendered_text_y = placed_pos(self.target_surface, self.rendered_text,
                                                                self.x_pos, self.y_pos)

    def __call__(self):
        """ return a rect of the modified area for dirty rect update
        """
        # surprisingly efficient no matter the string size, since python compare the hash
        if self.text != self.previous_text:
            self.rendered_text = render_text(self.text, self.text_color, self.text_font)
            self.update_pos()
            self.previous_text = self.text
        return self.target_surface.blit(self.rendered_text, (self.rendered_text_x, self.rendered_text_y))


class fps_counter_class: # TODO
    """
    """
    pass

# scene stuff
class scene_manager_class():
    """ scenes_dict : a dict scene.name : scene obj (assumed to be ordered, python >= 3.7)
        scene_list : a list of scene name
        active_scene_index : the index in scene list of the currently active scene
        active_scene_name : the name of the currently active scene
        active_scene : a scene object, the one run when calling the .run method
        widgets_manager

        #TODO: clean the mess with the diff between widgets/elements
        #      and unify next/previous/goto_scene
    """
    def __init__(self, scenes=None):
        self.widgets_manager = widgets_manager_class()

        if scenes is None: self.scenes_dict = {'default':scene_class('default')}
        else: self.scenes_dict = {scene.name : scene for scene in scenes}
        self.scenes_list = list(self.scenes_dict)
        self.nb_scenes = len(self.scenes_list)
        self.active_scene_index = 0
        self.active_scene_name = self.scenes_list[self.active_scene_index]
        self.active_scene = self.scenes_dict[self.active_scene_name]

        self.widgets_manager.active_widgets.clear()
        self.widgets_manager.active_widgets = self.active_scene.widgets.copy() # shallow copy, var in widgets should be conserved after clear ?

    #def add(self, scene):
    #    self.scenes_dict[scene.name] = scene
    #    self.scenes_list = list(self.scenes_dict)

    def next_scene(self):
        """ make the next scene in order the active scene
            If called at the end of the list, loop.
        """
        for action in self.active_scene.exit_actions: action()
        self.active_scene_index += 1
        if self.active_scene_index >= self.nb_scenes:
            self.active_scene_index = 0
        self.active_scene_name = self.scenes_list[self.active_scene_index]
        self.active_scene = self.scenes_dict[self.active_scene_name]
        for action in self.active_scene.entry_actions: action()

        self.widgets_manager.active_widgets.clear()
        self.widgets_manager.active_widgets = self.active_scene.widgets.copy() # shallow copy, var in widgets should be conserved after clear ?
        # fix for widget/elements incorrectly placed when changing scene after resizing window
        for widget in self.widgets_manager.active_widgets: widget.update_pos()
        for element in self.active_scene.elements:
            if hasattr(element, 'update_pos'): element.update_pos()

    def previous_scene(self):
        """ make the previous scene in order the active scene
            If called at the start of the list, loop.
        """
        for action in self.active_scene.exit_actions: action()
        self.active_scene_index -= 1
        if self.active_scene_index >= self.nb_scenes:
            self.active_scene_index = self.nb_scenes - 1
        self.active_scene_name = self.scenes_list[self.active_scene_index]
        self.active_scene = self.scenes_dict[self.active_scene_name]
        for action in self.active_scene.entry_actions: action()

        self.widgets_manager.active_widgets.clear()
        self.widgets_manager.active_widgets = self.active_scene.widgets.copy() # shallow copy, var in widgets should be conserved after clear ?
        # fix for widget/elements incorrectly placed when changing scene after resizing window
        for widget in self.widgets_manager.active_widgets: widget.update_pos()
        for element in self.active_scene.elements:
            if hasattr(element, 'update_pos'): element.update_pos()

    def goto_scene(self, scene_name):
        """ make the previous scene in order the active scene
            If called at the start of the list, loop.
        """
        if scene_name in self.scenes_dict:
            for action in self.active_scene.exit_actions: action()
            self.active_scene_index = self.scenes_list.index(scene_name)
            self.active_scene_name = self.scenes_list[self.active_scene_index]
            self.active_scene = self.scenes_dict[self.active_scene_name]
            for action in self.active_scene.entry_actions: action()

            self.widgets_manager.active_widgets.clear()
            self.widgets_manager.active_widgets = self.active_scene.widgets.copy() # shallow copy, var in widgets should be conserved after clear ?
            # fix for widget/elements incorrectly placed when changing scene after resizing window
            for widget in self.widgets_manager.active_widgets: widget.update_pos()
            for element in self.active_scene.elements:
                if hasattr(element, 'update_pos'): element.update_pos()
        else: raise ValueError("{scene_name} is not a existing scene.")

    def run(self, i_events):
        """ i_events : inputs events from pygame
        """
        self.active_scene.controller(i_events)

        for element in self.active_scene.elements:
            element()

        self.widgets_manager.process_events(i_events)
        self.widgets_manager.render()

class scene_class():
    """ name : a unique string that describe the scene
        elements : a dynamic iterable of elements, elements possess a __call__() method and are
                   to be called each frame if the scene is active
        widgets
        entry_action and exit_action :  expected to be static,
                                        iterable of functions to be called with no arguments
        controller : not quite a widget, not quite a elements, if you need it should be the high level
                     object that handle the scene as a whole logic-wise.
                     Will be called with pygame event as a arg if not None.
    """
    def __init__(self, name, elements=None, widgets=None,
                 entry_actions=None, exit_actions=None,
                 controller=None):
        self.name = name
        if elements is None: self.elements = []
        else: self.elements = elements
        if widgets is None: self.widgets = []
        else: self.widgets = widgets

        if entry_actions is None: self.entry_actions = []
        else: self.entry_actions = entry_actions
        if exit_actions is None: self.exit_actions = []
        else: self.exit_actions = exit_actions
        if controller is None: self.controller = lambda e: None
        else: self.controller = controller





if __name__ == '__main__':
    pygame.init()
    clock = pygame.time.Clock()
    pygame.display.set_caption("Tests utils :")


    text_widget = text_input_box_class(0.5, 0.1,
                                       button_surfaces(button_surface(color=ALMOST_BLACK,
                                                                      l=256, h=32)
                                                       )
                                       )

    start_button = button_class(0.5, 0.4,
                                button_surfaces(button_surface(),
                                                button_surface(outline_color=GREEN)
                                                ),
                                text = 'START',
                                action = lambda s: scene_manager.goto_scene('game')
                                )

    def change_text(s):
        if s.text == 'PATATE':
            s.text = 'OPTIONS'
        else: s.text = 'PATATE'

    options_button = button_class(0.5, 0.6,
                               button_surfaces(button_surface(),
                                               button_surface(outline_color=GREEN)
                                               ),
                               text = 'OPTIONS',
                               action = change_text
                               )

    #menu = windows_class(0.5, 0.5,
    #                     surface=pygame.Surface((256, 256), flags=SRCALPHA)
    #                     widgets=[start_button,
    #                              options_button],
    #                     movable=False
    #                     resizable=True)

    scenes = (scene_class(name='main_menu',
                          widgets=[text_widget,
                                   start_button,
                                   options_button]
                          ),
              scene_class(name='game'
                          )
              )


    scene_manager = scene_manager_class(scenes)

    running = True
    while running:
        events = pygame.event.get()

        remaining_events = []
        for event in events:
            if (event.type == pygame.QUIT):
                running = False
            else: remaining_events.append(event)

        # clear the screen before rendering :
        screen.fill(BLACK)

        scene_manager.run(remaining_events)

        clock.tick(60)
        rendered_fps = base_font.render(str(int(clock.get_fps())), False, WHITE)
        screen.blit(rendered_fps, (32, 32))
        pygame.display.flip()

    pygame.quit()









