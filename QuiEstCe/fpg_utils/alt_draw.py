"""
a couple of notes on differences between the original pygame.draw and this module :
- drawn shape are expected to always fit completely inside their constructor bounding rect,
  hence outlines 'grow' inside the shapes
  (ofc, with lines as a exception since 'inside' doesn't make sense then)
- stuff like being filled, or drawn with aa are optional parameters of the basic functions,
  not completely different ones (yes this is slower, see next point)

summary :

SSAA

def draw
#def ssaa_rect
def rect
def polygon
#def aafilled_circle
#def ssaa_circle
def circle
def ellipse
def arc
def line
def lines
"""
import pygame
from pygame.locals import SRCALPHA
from pygame import draw as draw_c

# consts :
SSAA = 1

# defs :
def draw(surface, target_surface, x, y, alpha_target=None):
    """ pygame.draw methods overwrite the target surface on the relevant pixels,
        unlike blit, which blend them (no matter the special_flag).
        This function imitate pygame.draw methods on this, but for surface.
        This is pretty meaningful when surface with alpha are concerned, otherwise juste
        use blit, it will be much more efficient.
        Hence, surface is expected to be a RGBA pygame surface.

        If alpha_target is a valid alpha value, pixels from surface verifying 0 < alpha < alpha_target
        will be merged with pixels from target surface, while pixels with alpha >= alpha_target will
        overwrite as usual the pixels from target_surface.
        It's useful for example if surface is a image with global transparency, but also as aa and you
        want it to behave as you'd expect, merging with the bg to make the outline smooth.
    """
    l, h = surface.get_size()
    if alpha_target is None:
        for px_x in range(l):
            for px_y in range(h):
                color = surface.get_at((px_x, px_y))
                if color[3] > 0: # if the pixel isn't completely transparent
                    target_surface.set_at((x + px_x, y + px_y), color)
    else:
        for px_x in range(l):
            for px_y in range(h):
                target_px_coord = (x + px_x, y + px_y)
                color = surface.get_at((px_x, px_y))
                alpha = color[3]
                if alpha > 0: # if the pixel isn't completely transparent
                    if alpha < alpha_target:
                        target_px_color = target_surface.get_at(target_px_coord)
                        target_surface.set_at(target_px_coord,
                                              (round((target_px_color[0] + color[0]) / 2),
                                               round((target_px_color[1] + color[1]) / 2),
                                               round((target_px_color[2] + color[2]) / 2),
                                               round((target_px_color[3] + color[3]) / 2)
                                               )
                                              )
                    else: target_surface.set_at(target_px_coord, color)

def rect(target_surface, color, rect, width=0, border_radius=1,
         border_top_left_radius=-1, border_top_right_radius=-1, border_bottom_left_radius=-1, border_bottom_right_radius=-1,
         aa=False):
    """ pygame.draw.rect with a border_radius != 0, but with 2x ssaa,
        Note, if you don't want rounded corners, there is no point in using this compared to
        pygame.draw.rect, hence why this function assume a border radius.
        Note (bis), unlike pygame.draw.rect, width > 1 extend inward, hence a ssaa_rect will always
        fit inside the expected bounding_box.
        follow pygame.draw.rect format for args
        TODO : add the possibility of having different radius or each corner

        surface (Surface) --
        surface to draw on

        color (Color or int or tuple(int, int, int, [int])) --
        color to draw with, the alpha value is optional if using a tuple (RGB[A])

        rect (Rect) --
        rectangle to draw, position and dimensions

        width (int) --
        (optional) used for line thickness or to indicate that the rectangle is to be filled
        (not to be confused with the width value of the rect parameter)

        if width == 0, (default) fill the rectangle
        if width > 0, used for line thickness
        if width < 0, nothing will be drawn

        border_radius (int) --
        (optional) used for drawing rectangle with rounded corners.
        The supported range is [0, min(height, width) / 2],
        with 0 representing a rectangle without rounded corners.

        border_top_left_radius (int) --
        (optional) used for setting the value of top left border.
        If you don't set this value, it will use the border_radius value.

        border_top_right_radius (int) --
        (optional) used for setting the value of top right border.
        If you don't set this value, it will use the border_radius value.

        border_bottom_left_radius (int) --
        (optional) used for setting the value of bottom left border.
        If you don't set this value, it will use the border_radius value.

        border_bottom_right_radius (int) --
        (optional) used for setting the value of bottom right border.
        If you don't set this value, it will use the border_radius value.

        if border_radius < 1 it will draw rectangle without rounded corners
        if any of border radii has the value < 0 it will use value of the border_radius
        (removed old behavior:
        If sum of radii on the same side of the rectangle is greater than the rect size the radii will get scaled,
        now only a range is supported for radius.)

        Returns:
        a rect bounding the changed pixels, should be equal to the given rect parameter
    """
    x, y, l, h = rect
    # it's not to opti by drawing only the necessary components (since it's actually slower), but to avoid
    # the issue when color as alpha and radius is double l or h, lines appear because of overdrawing.
    radius_is_double_l = (border_radius * 2 == l)
    radius_is_double_h = (border_radius * 2 == h)
    left_center_x = x + border_radius
    right_center_x = x + l - border_radius
    top_center_y = y + border_radius
    bottom_center_y = y + h - border_radius
    central_rect_l = right_center_x - left_center_x
    central_rect_h = bottom_center_y - top_center_y

    if width > 1:
        radius_minus_width = border_radius - width
        # top rect
        if not radius_is_double_l:
            draw_c.rect(target_surface, color, (left_center_x, y, central_rect_l, width))
        # right rect
        if not radius_is_double_h:
            draw_c.rect(target_surface, color, (right_center_x + radius_minus_width, top_center_y, width, central_rect_h))
        # bottom rect
        if not radius_is_double_l:
            draw_c.rect(target_surface, color, (left_center_x, bottom_center_y + radius_minus_width, central_rect_l, width))
        # left rect
        if not radius_is_double_h:
            draw_c.rect(target_surface, color, (x, top_center_y, width, central_rect_h))
    else:
        # central rect
        if not (radius_is_double_h or radius_is_double_l):
            draw_c.rect(target_surface, color, (left_center_x, top_center_y, central_rect_l, central_rect_h))
        # top rect
        if not radius_is_double_l:
            draw_c.rect(target_surface, color, (left_center_x, y, central_rect_l, border_radius))
        # right rect
        if not radius_is_double_h:
            draw_c.rect(target_surface, color, (right_center_x, top_center_y, border_radius, central_rect_h))
        # bottom rect
        if not radius_is_double_l:
            draw_c.rect(target_surface, color, (left_center_x, bottom_center_y, central_rect_l, border_radius))
        # left rect
        if not radius_is_double_h:
            draw_c.rect(target_surface, color, (x, top_center_y, border_radius, central_rect_h))

    # top left disc
    #aafilled_circle(target_surface, left_center_x, top_center_y, border_radius, color)
    ssaa_circle(target_surface, color, (left_center_x, top_center_y), border_radius, width, draw_top_left=True)
    # top right disc
    #aafilled_circle(target_surface, right_center_x, top_center_y, border_radius, color)
    ssaa_circle(target_surface, color, (right_center_x, top_center_y), border_radius, width, draw_top_right=True)
    # bottom left disc
    #aafilled_circle(target_surface, left_center_x, bottom_center_y, border_radius, color)
    ssaa_circle(target_surface, color, (left_center_x, bottom_center_y), border_radius, width, draw_bottom_left=True)
    # bottom right disc
    #aafilled_circle(target_surface, right_center_x, bottom_center_y, border_radius, color)
    ssaa_circle(target_surface, color, (right_center_x, bottom_center_y), border_radius, width, draw_bottom_right=True)

def ssaa_circle(target_surface, color, center, radius, width=0,
                draw_top_right=False, draw_top_left=False, draw_bottom_left=False, draw_bottom_right=False):
    """ the same as pygame.draw.circle, but anti aliased through 2x super sampling
        follow pygame.draw.circle format for args
        Note, unlike pygame.draw.circle, width > 1 extend inward, hence a ssaa_circle will always
        fit inside the expected bounding_box.
    """
    diameter = radius + radius
    ss_size = diameter + diameter
    surface_to_shrink = pygame.Surface((ss_size, ss_size), flags=SRCALPHA)

    # otherwise with default black bg, smoothscale makes a dark outline when merging pixels
    #if len(color) == 3: surface_to_shrink.fill((*color, 0))
    #elif len(color) == 4: surface_to_shrink.fill((*color[0:3], 0))
    #else: raise ValueError("Expected a iterable with RGB, or RGBA format for color.")
    bg_color = (*color[0:3], 0)
    surface_to_shrink.fill(bg_color)

    if width > 1:
        draw_c.circle(surface_to_shrink, color, (diameter, diameter), diameter, 0,
                           draw_top_right, draw_top_left, draw_bottom_left, draw_bottom_right)
        draw_c.circle(surface_to_shrink, bg_color, (diameter, diameter), diameter - (width * 2), 0,
                           draw_top_right, draw_top_left, draw_bottom_left, draw_bottom_right)
    else:
        draw_c.circle(surface_to_shrink, color, (diameter, diameter), diameter, width,
                           draw_top_right, draw_top_left, draw_bottom_left, draw_bottom_right)

    x, y = center
    if len(color)>3:
        # if alpha, to mimic the overwriting behavior of pygame.draw methods,
        # despite being slower, draw() is needed instead of blit
        draw(pygame.transform.smoothscale(surface_to_shrink, (diameter, diameter)), target_surface, x - radius, y - radius, color[3])
    else:
        target_surface.blit(pygame.transform.smoothscale(surface_to_shrink, (diameter, diameter)), (x - radius, y - radius))


if __name__ == '__main__':
    pygame.init()
    screen = pygame.display.set_mode((480, 360))
    clock = pygame.time.Clock()

    BLACK = (0, 0, 0)
    GRAY = (128, 128, 128)
    WHITE = (255, 255, 255)
    RED = (255, 0, 0)
    GREEN = (0, 255, 0)
    BLUE = (0, 0, 255)

    run = True
    while run:
        clock.tick(60)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False

        screen.fill(GRAY)

        rect(screen, GREEN, (16, 16, 128, 64),
             border_top_left_radius=0,
             border_top_right_radius=16,
             border_bottom_left_radius=32,
             border_bottom_right_radius=8,
             aa=SSAA)





        pygame.display.flip()

    pygame.quit()
    exit()
