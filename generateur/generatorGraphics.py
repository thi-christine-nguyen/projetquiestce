import tkinter as tk
from email.mime import image
from tkinter import ttk
from tkinter import filedialog 
from ComboPicker import *
from generateur import Generateur



class Personnage():
    def __init__(self):
        self.name = ""
        self.image = "blank.png"
        self.attributs = {}
    def __init__(self, name, photo, attributs):
        self.name = name
        self.image = photo
        self.attributs = attributs
    
    def getPerso(self):
        return [self.name, self.image, self.attributs]

class Grille():
    def __init__(self):

        '''generals'''
        self.name = ""
        self.nombre_lignes = 0
        self.nombre_colonnes = 0

        '''categories'''
        self.categories = []
        self.attributs = {}

        '''personnages'''
        self.personnages = {}

        self.generateur = Generateur(self.name)

    '''generals'''
    def setName(self, name):
        self.name = name

    def getName(self):
        return self.name
    
    '''categories'''
    def getCategories(self):
        return self.categories

    def setCategories(self, categorie):
        self.categories.append(categorie)
        self.attributs[categorie] = {}
    
    def getAttributs(self):
        return self.attributs

    def setAttributs(self, categorie, attributs):
        self.attributs[categorie] = attributs
    
    '''personnages'''
    def setPersonnage(self, name) :
        self.personnages[name] = Personnage()

    def setPersonnage(self, name, photo, attributs) :
        self.personnages[name] = Personnage(name, photo, attributs)

    def getPersonnages(self):
        list = {}
        for perso in self.personnages :
            list[perso] =  self.personnages[perso].getPerso()
        return list

BG_TAB_COLOR = '#202225'

class add_tab_button_class():
    def __init__(self, __manager, parent_frame):
        self.__manager = __manager
        self.add_tab_frame = tk.Frame(parent_frame, bg = BG_TAB_COLOR)

        self.close_button = tk.Button(self.add_tab_frame,
                                      fg='black',
                                      bg='white')
        self.close_button["text"] = "+"
        self.close_button["command"] = self.add_tab
        self.close_button.pack(side='left', padx=5, pady=5)

    def add_tab(self):
        self.__manager.add_tab(tk.Frame(self.__manager.parent_frame, bg = 'gray'), self.__manager.id_index, can_close=False)
        self.__manager.id_index += 1

class tab_class():
    """ tab_frame : the clickable tab in the ribon
        frame : the frame displayed when the tad is selected
    """
    def __init__(self, __manager, parent_frame, frame, name='', selected=False, can_close=True):
        self.__manager = __manager
        self.name = name
        self.frame = frame
        self.selected = False

        self.tab_frame = tk.Frame(parent_frame, bg = BG_TAB_COLOR)
        #self.frame.pack()

        self.button = tk.Button(self.tab_frame,
                                fg='black',
                                bg='white')
        self.button["text"] = self.name
        self.button["command"] = self.select_tab
        self.button.pack(side='left', padx=5, pady=5)

        if can_close:
            self.close_button = tk.Button(self.tab_frame,
                                          fg='black',
                                          bg='white')
            self.close_button["text"] = "x"
            self.close_button["command"] = self.delete_tab
            self.close_button.pack(side='left', padx=5, pady=5)

    def change_name(self, name):
        if self.name == self.__manager.selected_tab:
            self.__manager.selected_tab = name
        self.button.configure(text = name)
        del self.__manager.tabs[self.name]
        self.name = name
        self.__manager.tabs[name] = self
        
    def select_tab(self):
        self.__manager.select_tab(self.name)

    def delete_tab(self):
        grille.generateur.delCategorie(self.name)
        self.tab_frame.pack_forget()
        self.frame.pack_forget()
        del self.__manager.tabs[self.name]
        
        tab = self.category_ribon.tabs[self.category_ribon.selected_tab]

        if self.persos_ribon.tabs != None : 
            chara_tabs = self.persos_ribon.tabs
        for chara_tab in chara_tabs :
            for frame in chara_tabs[chara_tab].liste_frame_pickers :
                frame.destroy()
            chara_tabs[chara_tab].liste_frame_pickers = []
        
        


class tabs_manager_class():
    """ if plus_button is True, a crea_method must be passed which should be able to take the tab as a argument.
    """
    def __init__(self, parent_frame=None, plus_button=False, crea_method=None):
        self.parent_frame = parent_frame
        if parent_frame is None: self.frame = tk.Frame(bg = 'white')
        else: self.frame = tk.Frame(parent_frame, bg = 'white')
        self.frame.pack(anchor='nw',
                        fill = tk.X)

        self.plus_button = plus_button
        self.crea_method = crea_method


        self.id_index = 0
        self.tabs = dict()
        #if plus_button:
        #    self.tabs.append(add_tab_button)

        for tab_name in self.tabs:
            self.tabs[tab_name].tab_frame.pack(anchor='nw', padx=2, pady=2)

        self.selected_tab = None

        if self.plus_button:
            self.add_tab_button = add_tab_button_class(self, self.frame)
            self.add_tab_button.add_tab_frame.pack(anchor='nw', padx=2, pady=2)

    def select_tab(self, name):
        if (not (self.selected_tab is None)) and (self.selected_tab in self.tabs):
            self.tabs[self.selected_tab].frame.pack_forget()
            self.tabs[self.selected_tab].selected = False
        self.tabs[name].frame.pack(anchor='n',
                                   fill = tk.BOTH,
                                   expand=True)
        self.tabs[name].selected = True
        self.selected_tab = name

    def add_tab(self, frame, name, can_close):
        # remove the + button to always have it at the end of the tab list
        if self.plus_button:
            self.add_tab_button.add_tab_frame.pack_forget()

        self.tabs[name] = tab_class(self, self.frame, frame, name, can_close=can_close)
        self.tabs[name].tab_frame.pack(anchor='nw', side='left', padx=2, pady=2)

        # put the + button back if needed
        if self.plus_button:
            self.crea_method(self.tabs[name])
            self.add_tab_button.add_tab_frame.pack(anchor='nw', padx=2, pady=2)


class Application(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master=master)
        self.master = master
        self.master.configure(bg = 'blue')
        #self.pack(fill = tk.BOTH,
        #          expand=True)
        self.create_widgets()
 
    def create_widgets(self):

        ### grille frame :

        def grilleButtonFunction(event=None):
        
            '''TODO :
                -test si le nom de grille a déja été utilisé
                -remplacer le test actuel par ce test
            '''
            ajoute = False
            if not ajoute :
                result_grid_label.config(text="Le nom a été ajouté")
                grille.setName(grid_name)
                grille.generateur.setType(grid_name.get())
                grille_reset()
            else : 
                result_grid_label.config(text="Le nom de grille existe déjà")
        
        def grille_reset():
            grid_name_entry.delete(0, "end")
            result_grid_label.text = ''
        
        self.grille_frame = tk.Frame(bg = 'gray')
        tk.Label(self.grille_frame,
                 text = 'Choisissez le nom de votre grille :',
                 bg = 'gray').pack(fill = tk.BOTH)

        self.sub_grille_frame=tk.Label(self.grille_frame, bg='gray')
        self.sub_grille_frame.pack()
    
        grid_name = tk.StringVar()
        grid_name_entry = tk.Entry(self.sub_grille_frame, textvariable=grid_name)
        grid_name_entry.pack(side='left')
        grid_name_entry.focus()

        validate_grid_button = tk.Button(self.sub_grille_frame, text='Valider')
        validate_grid_button.pack(side='right')
        validate_grid_button.configure(command=grilleButtonFunction)

        result_grid_label = tk.Label(self.grille_frame, text = "", bg='gray')
        result_grid_label.pack()

        ### categories frame :

        self.categories_frame = tk.Frame(bg = 'Gray')

        def populate_categories_creation_widget(tab):

            '''crée les widgets dans les tabs'''
            
            tab.categorie = ''
            tab.liste_attributs = []
            tab.cate_ajoute = False

            def categoryButtonFunction(event=None):
        
                '''
                appelé par le bouton validate_category_button
                TODO :
                    -test si le nom de grille a déja été utilisé
                    -remplacer le test actuel par ce test
                '''
                tab = self.category_ribon.tabs[self.category_ribon.selected_tab]
                if not tab.cate_ajoute :
                    result_category_label.config(text="La catégorie a été ajoutée")
                    grille.setCategories(category_name.get())
                    grille.generateur.addCategories(category_name.get())
                    
                    tab.change_name(category_name.get())
                    tab.categorie = category_name.get()
                    category_reset()
                    tab.cate_ajoute = True
                else : 
                    result_category_label.config(text="Cette categorie existe déjà")
        
            def category_reset():
                category_name_entry.delete(0, "end")
                result_category_label.text = ''
            
            def attributeButtonFunction(event=None):
                
                '''
                fonction appelée par le bouton validate_category_button
                TODO :
                    -test si le nom de grille a déja été utilisé
                    -remplacer le test actuel par ce test
                '''
                
                tab = self.category_ribon.tabs[self.category_ribon.selected_tab]
                if self.persos_ribon.tabs != None : 
                    chara_tabs = self.persos_ribon.tabs
                tab.ajoute = False
                if not tab.ajoute :
                    tab.liste_attributs.append(attribute_name.get())
                    grille.setAttributs(tab.categorie, tab.liste_attributs)
                    grille.generateur.addAttByCat(tab.categorie, attribute_name.get())
                   
                    result_attribute_label.config(text=tab.liste_attributs)
                    
                    for chara_tab in chara_tabs :
                        for frame in chara_tabs[chara_tab].liste_frame_pickers :
                            frame.destroy()
                        chara_tabs[chara_tab].liste_frame_pickers = []


                        for i in range(len(grille.getCategories())):
                            
                    
                            OptionList = grille.getAttributs()[grille.getCategories()[i]] 

                            picker_frame=ttk.Frame(chara_tabs[chara_tab].sub_param_frame, height=20, width=100)
                            picker_frame.grid_propagate(False)
                            picker_frame.pack()
                            tk.Label(picker_frame, text = grille.getCategories()[i], bg='gray').pack(fill = tk.BOTH)

                            opt = Combopicker(picker_frame, OptionList)
                            opt.config(width=90, font=('Helvetica', 12))
                            opt.pack()

                            chara_tabs[chara_tab].liste_frame_pickers.append(picker_frame)

                        #print(chara_tabs[chara_tab].liste_pickers, chara_tabs[chara_tab].liste_frame_pickers)
                    attribute_reset()
                    
                else : 
                    result_attribute_label.config(text="Le nom de grille existe déjà")
            
            def attribute_reset():
                attribute_name_entry.delete(0, "end")
                result_attribute_label.text = ''

            tab.change_name('nouvelle categorie')

            tk.Label(tab.frame,
                text = 'nom de la categorie :', bg = 'gray').pack(fill = tk.BOTH)
        
            tab.sub_category_frame=tk.Frame(tab.frame, bg='gray')
            tab.sub_category_frame.pack()

            category_name = tk.StringVar()
            category_name_entry = tk.Entry(tab.sub_category_frame, textvariable=category_name)
            category_name_entry.pack(side='left')
            category_name_entry.focus()

            validate_category_button = tk.Button(tab.sub_category_frame, text='Valider')
            validate_category_button.pack(side='right')
            validate_category_button.configure(command=categoryButtonFunction)

            result_category_label = tk.Label(tab.frame, text = "", bg='gray')
            result_category_label.pack(fill = tk.BOTH)



            tab.sub_attribute_frame=tk.Frame(tab.frame, bg='gray')
            tab.sub_attribute_frame.pack()
        
            attribute_name = tk.StringVar()
            attribute_name_entry = tk.Entry(tab.sub_attribute_frame, textvariable=attribute_name)
            attribute_name_entry.pack(side='left')
            attribute_name_entry.focus()

            validate_attribute_button = tk.Button(tab.sub_attribute_frame, text='Valider')
            validate_attribute_button.pack(side='right')
            validate_attribute_button.configure(command=attributeButtonFunction)

            result_attribute_label = tk.Label(tab.frame, text = "", bg='gray')
            result_attribute_label.pack(fill = tk.BOTH)



        self.category_ribon = tabs_manager_class(self.categories_frame, plus_button=True,
                                          crea_method=populate_categories_creation_widget)
        self.category_ribon.frame.pack(side='top')

        '''creation de la frame contenant onglets, pack au moment de la creation'''
        ### persos frame :
        self.persos_frame = tk.Frame(bg = 'Gray')

        def populate_perso_creation_widget(tab):
            tab.change_name('nouvelle case')
            
            tab.perso_ajoute = False
            tab.photo = ""
            tab.parametres_perso = []
            
            '''recuperation du nom'''

            def nameButton():
                '''TODO :
                    -test si le nom de grille a déja été utilisé
                    -remplacer le test actuel par ce test
                '''
                if not tab.perso_ajoute :
                    result_name_label.config(text="Le nom a été ajouté")
                    tab.change_name(perso_name.get())
                   
                    grille.generateur.noms.append(perso_name.get())
                    tab.name = perso_name.get()
                    name_reset()
                    tab.perso_ajoute = True
                else : 
                    result_name_label.config(text="Le nom de grille existe déjà")
            def name_reset():
                #perso_name_entry.delete(0, "end")
                result_name_label.text = ''
            
            '''recuperation de la photo'''
            def browseFiles(): 
                filename = filedialog.askopenfilename(initialdir = "/", 
                                                    title = "Select a File", 
                                                    filetypes = (("images files", 
                                                                    "*.png*"), 
                                                                ("all files", 
                                                                    "*.*"))) 
                
                chemin=filename.split("/")
                tab.photo = chemin[-1]
                result_name_label.configure(text="image: "+chemin[-1])
                
            '''mis dans la commande de creation de perso'''
            def createPersoButton():
                '''TODO :
                    -test si le nom de grille a déja été utilisé
                    -remplacer le test actuel par ce test
                '''
                ajouté = False
                if not ajouté :
                    result_name_label.config(text="la case a été créée")
                    res = {}
                    for i in range(len(tab.parametres_perso)):
                        res[grille.getCategories()[i]] = tab.parametres_perso[i].current_value.split(",")
                    grille.setPersonnage(tab.name, tab.photo, res)
                    perso_reset()
                    
                    dic = {} 

                    for i in range(len(grille.getPersonnages())) : 
                        
                        dic['nom'] = grille.getPersonnages()[tab.name][0]
                        dic['fichier'] = grille.getPersonnages()[tab.name][1]
                        g=grille.getPersonnages()[tab.name][2]
                        ki = g.keys()
                        for k in ki : 
                            dic[k] = g[k]
                    if grille.generateur.createData(dic) == False : 
                        warning = tk.Tk()
                        tk.Label(warning, text = "Attention !! Cette case existe déjà. Veuillez relancer le générateur").pack(fill = tk.BOTH)
                    
                else : 
                    result_name_label.config(text="Le nom de grille existe déjà")
            def perso_reset():
                result_name_label.text = ''
            
            tab.change_name('nouvelle case')
            tk.Label(tab.frame,text = 'choisis un nom :', bg = 'gray').pack(fill = tk.BOTH)

            '''attribution du nom'''

            tab.sub_name_frame=tk.Frame(tab.frame, bg='gray')
            tab.sub_name_frame.pack()

            perso_name = tk.StringVar()
            perso_name_entry = tk.Entry(tab.sub_name_frame, textvariable=perso_name)
            perso_name_entry.pack(side='left')
            perso_name_entry.focus()

            validate_perso_button = tk.Button(tab.sub_name_frame, text='Valider')
            validate_perso_button.pack(side='right')
            validate_perso_button.configure(command=nameButton)


            '''attribution de la photo'''
            
            tab.sub_photo_frame=tk.Frame(tab.frame, bg='gray')
            tab.sub_photo_frame.pack()

            explore_photo_button = tk.Button(tab.sub_photo_frame,  
                        text = "Parcourir les fichiers", 
                        command = browseFiles)
            explore_photo_button.pack()



            '''attribution des parametres'''

            tab.sub_param_frame=tk.Frame(tab.frame, bg='gray')
            tab.sub_param_frame.pack()

            tab.liste_pickers = []
            tab.liste_frame_pickers = []

            def create_picker():
                print("created")
                for i in range(len(grille.getCategories())):
                    
                    OptionList = grille.getAttributs()[grille.getCategories()[i]] 

                    picker_frame=ttk.Frame(tab.sub_param_frame, height=20, width=100)
                    picker_frame.grid_propagate(False)
                    picker_frame.pack()
                    tk.Label(picker_frame, text = grille.getCategories()[i], bg='gray').pack(fill = tk.BOTH)

                    opt = Combopicker(picker_frame, OptionList)
                    opt.config(width=90, font=('Helvetica', 12))
                    opt.pack()

                    tab.liste_pickers.append(opt)
                    tab.liste_frame_pickers.append(picker_frame)

                    tab.parametres_perso.append(opt)
            create_picker()

            result_name_label = tk.Label(tab.frame, text = "", bg='gray')
            result_name_label.pack(fill = tk.BOTH)

            button = tk.Button(tab.frame, text='Ajouter')
            button.pack()
            button.configure(command =createPersoButton)

            

        self.persos_ribon = tabs_manager_class(self.persos_frame, plus_button=True,
                                          crea_method=populate_perso_creation_widget)
        self.persos_ribon.frame.pack(side='top')

        ###validation frame :

        def validationButtonFunction(event=None):
        
            '''TODO :
            -test si le nom de grille a déja été utilisé
            -remplacer le test actuel par ce test
            '''
            ajouté = False
            if not ajouté :
                result_validate_label.config(text="Le fichier a été créé")
                grille.generateur.setLigneColonne()
                grille.generateur.sauvegarde()
                validate_reset()
            else : 
                result_validate_label.config(text="Le nom de grille existe déjà")
        
        def validate_reset():
            result_validate_label.text = ''
        
        self.validate_frame = tk.Frame(bg = 'gray')
        tk.Label(self.validate_frame,
                 text = 'Etes-vous sûr de valider ?  :',
                 bg = 'gray').pack(fill = tk.BOTH)

        validation_validate_button = tk.Button(self.validate_frame, text='Valider')
        validation_validate_button.pack()
        validation_validate_button.configure(command=validationButtonFunction)

        result_validate_label = tk.Label(self.validate_frame, text = "", bg='gray')
        result_validate_label.pack()


        main_ribon = tabs_manager_class()
        main_ribon.add_tab(self.grille_frame, 'grille', can_close=False)
        main_ribon.add_tab(self.categories_frame, 'categories', can_close=False)
        main_ribon.add_tab(self.persos_frame, 'case', can_close=False)
        main_ribon.add_tab(self.validate_frame, 'Validation', can_close=False)
        main_ribon.frame.pack(side='top')

        main_ribon.select_tab('grille')



root = tk.Tk()
app = Application(master=root)
grille=Grille()

app.master.title("generateur de JSON")
app.mainloop()
