import cmath
from function import *
import math as m
import copy
import ast 
import json
import sys
import os
from pathlib import Path
import sys
import platform

slash=""
if platform.system()=="Windows":
    slash="\\"
else:
    slash="/"

sys.path.append(os.path.abspath(os.path.join('..', 'Jeu')))
#from Game import function

class Generateur :
    #constructeur

    def __init__(self, type):
        self.type = type
        self.images = ""
        self.ligne = 0
        self.colonne = 0
        self.categories = []
        self.attributs ={}
        self.noms = []
        self.possibilites = {}
        self.blanks=0


    def setType(self, type) :
        #personnage, animaux...
        self.type = type

    def setImagePath(self, path) :
        #nom du dossier : ../images/personnages
        self.images = path

    def setLigneColonne(self) :
        n=len(self.possibilites)
        rac= m.sqrt(n)
        l=m.ceil(n/5)
        blanks=5*l - n
        if n==8 or n==7:
            self.colonne=4
            self.ligne=2
            self.blanks = self.ligne * self.colonne - n
        elif n==9:
            self.colonne=3
            self.ligne=3
        elif (blanks>2 and n<=16) :
            self.ligne=m.floor(rac)
            self.colonne=m.ceil(rac)
            self.blanks=self.ligne*self.colonne-n
        else:
            self.colonne=5
            self.ligne=l
            self.blanks=blanks


    def addCategories(self, categorie) :
        #rajoute le nom des categories = cheveux, lunettes...
        self.categories.append(categorie)
        self.attributs[categorie] = []

      

    def delCategorie(self, categorie) : 
        if self.possibilites != {} : 
            if categorie in self.possibilites[0] or categorie in self.categories : 
                for i in self.possibilites : 
                    copie = dict(self.possibilites[i])
                    del copie[categorie]
                    self.possibilites[i] = copie

        self.categories.remove(categorie)
        del self.attributs[categorie]
        
    def delAttributByCat(self, categorie, attribut) : 
        for i in self.attributs : 
           if i == categorie : 
                if attribut in self.attributs[i] : 
                    self.attributs[i].remove(attribut)
        

       

    def addAttByCat(self, categorie, attribut) :
        #ajoute les attributs selon les categorie = cheveux -> blanc, noir...

        #si la categorie a deja une valeur
        if isinstance(attribut, list) :
            for i in attribut :
                self.attributs[categorie].append(i)
        else :
            self.attributs[categorie].append(attribut)




       

    def getAttByCat(self, categorie) :
        #recupere les attribut selon la categorie = [noir, blanc, roux...]
        return self.attributs[categorie]

    #si ajoute d'un nouvelle categorie = maj les datas
    def addCatData(self, personne, categorie, attribut) :
        for p in self.possibilites :
            if self.possibilites[p]["nom"] is personne :
                self.possibilites[p][categorie] = attribut
        
        for i in self.attributs : 
            if categorie == i : 
                if attribut not in self.attributs[i] : 
                    self.attributs[i].append(attribut)

            
    

    def extraitNom(self,dic) : 
        #retire les paramètres nom et fichier du dictionnaire
        copie = dic.copy()
        delete = []
        for p,q in copie.items():
            
            if p=="fichier" or p=="nom":
               delete.append(p)

        for i in delete:
             del copie[i]
        

        return copie

    
    def verifDoubleData(self,dataList):
        #permet de vérifier s'il y a des doublures dans les attribut (jeu perdant si oui)
        if dataList == self.possibilites : 
            return True


        for i in self.possibilites:
            if self.extraitNom(self.possibilites[i]) == self.extraitNom(dataList) : 
                return True
        

        return False
        

    
    def majAttributList(self, dataList) : 
        info = self.extraitNom(dataList)
        #self.possibilites[len(self.possibilites)] = dataList
        for i in info : 
            for j in self.attributs : 
                if i == j : 
                    if info[i] not in self.attributs[j] : 
                        self.attributs[j] = info[i]
                        
    
    def createData(self, dataList) :
        #Creation des cases (possibilites)
       
        if self.possibilites == {} : 
            self.possibilites[len(self.possibilites)] = dataList
            #self.majAttributList(dataList)
            


        elif (self.verifDoubleData(dataList)==True):

            #verifDoubleData si double appeler les fonctions addCategories, addAttByCat et
            #parcourir le tableau des possiblité et appeler addCatData pour mettre à jour chaque personnage
            return False
           
            
            
        else:
            self.possibilites[len(self.possibilites)] = dataList
            #self.majAttributList(dataList)
           
        

    def sauvegarde(self) :

        #Crée le fichier json (grille)

        jsonCopie = dict({"type" : self.type,
        "images" : self.images,
        "ligne" : self.ligne,
        "colonne" : self.colonne,
        "categories" : self.categories,
        "noms" : self.noms,
        "possibilites" : self.possibilites})


        for i in self.categories :
            jsonCopie[i] = self.attributs[i]

        path = Path("../QuiEstCe/FichiersJson/" + self.type + ".json").parent
        
        #with va automatiquement fermer le fichier à la fin de son utilisation
        with open(os.path.join(path,  self.type + ".json"), 'w') as f:
            f.write(json.dumps(jsonCopie, sort_keys=True, indent=4, separators=(',', ': ')))







